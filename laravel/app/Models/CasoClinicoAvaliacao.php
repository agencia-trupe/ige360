<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CasoClinicoAvaliacao extends Model
{
    protected $table = 'casos_clinicos_avaliacoes';

    protected $guarded = [];

    public function getNotaFormatadaAttribute()
    {
        return self::formataNota($this->nota);
    }

    public static function formataNota($nota)
    {
        return str_replace('.', ',', $nota + 0);
    }
}
