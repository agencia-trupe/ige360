<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use App\Notifications\ResetPassword;

class Medico extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $table = 'medicos';

    protected $guarded = ['id'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'convite_enviado_em'   => 'datetime:d/m/y H:i',
        'convite_reenviado_em' => 'datetime:d/m/y H:i',
        'cadastrado_em'        => 'datetime:d/m/y H:i',
        'termos_aceitos_em'    => 'datetime:d/m/y H:i',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword('medico.password.reset', $token));
    }

    public function grupo()
    {
        return $this->belongsTo(Grupo::class, 'grupo_id', 'id');
    }

    public function casoClinico()
    {
        return $this->hasOne(CasoClinico::class, 'medico_id');
    }

    public function scopeBusca($query, $value)
    {
        return $query
            ->where('nome', 'LIKE', "%$value%")
            ->orWhere('email', 'LIKE', "%$value%");
    }

    public function getPodeSubmeterCasoClinicoAttribute()
    {
        return
            $this->grupo->data_limite &&
            $this->grupo->data_limite->gte(Carbon::now()->startOfDay());
    }
}
