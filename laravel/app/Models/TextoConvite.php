<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class TextoConvite extends Model
{
    protected $table = 'texto_convite';

    protected $fillable = ['texto'];
}
