<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CasoClinico extends Model
{
    protected $table = 'casos_clinicos';

    protected $guarded = [];

    protected $casts = [
        'enviado_em'         => 'datetime:d/m/y H:i',
        'validado_em'        => 'datetime:d/m/y H:i',
        'distribuido_em'     => 'datetime:d/m/y H:i',
        'alteracao_admin_em' => 'datetime:d/m/y H:i',
    ];

    public function medico()
    {
        return $this->belongsTo(Medico::class, 'medico_id');
    }

    public function avaliacoes()
    {
        return $this->hasMany(CasoClinicoAvaliacao::class, 'caso_id');
    }

    public function setParticipantesAttribute($value)
    {
        $this->attributes['participantes'] = json_encode(array_slice($value, 0, 5));
    }

    public function getParticipantesAttribute($value)
    {
        return json_decode($value);
    }

    public function getNotaAttribute() {
        return $this->has('avaliacoes')
            ? CasoClinicoAvaliacao::formataNota($this->avaliacoes()->avg('nota'))
            : null;
    }

    public function scopeParaValidar($query)
    {
        return $query
            ->whereNotNull('enviado_em')
            ->whereNull('validado_em')
            ->orderBy('enviado_em', 'ASC');
    }

    public function scopeValidado($query, $id = null)
    {
        $query
            ->whereNotNull('enviado_em')
            ->whereNotNull('validado_em');

        if ($id) $query->where('validado_por', $id);

        return $query->orderBy('validado_em', 'ASC');
    }

    public function scopeParaDistribuir($query)
    {
        return $query
            ->whereNotNull('enviado_em')
            ->whereNotNull('validado_em')
            ->whereNull('distribuido_em');
    }

    public function scopeParaAvaliar($query, $id)
    {
        return $query
            ->whereNotNull('enviado_em')
            ->whereNotNull('validado_em')
            ->whereNotNull('distribuido_em')
            ->whereDoesntHave('avaliacoes', function($q) use ($id) {
                $q->where('avaliador_id', $id);
            })->orderBy('enviado_em', 'ASC');
    }

    public function scopeAvaliado($query, $id)
    {
        return $query
            ->whereNotNull('enviado_em')
            ->whereNotNull('validado_em')
            ->whereNotNull('distribuido_em')
            ->whereHas('avaliacoes', function($q) use ($id) {
                $q->where('avaliador_id', $id);
            })->orderBy('enviado_em', 'ASC');
    }

    public function scopeResultadosOrdenados($query, $amount = null)
    {
        /*
         * Calcula média das avaliações dos casos que foram avaliados
         * por todos os avaliadores e retorna quantidade de casos
         * ordenados da maior para a menor nota.
         */

        return $query
            ->with('medico')
            ->has('avaliacoes')
            ->join('casos_clinicos_avaliacoes', 'casos_clinicos_avaliacoes.caso_id', '=', 'casos_clinicos.id')
            ->select(['casos_clinicos.*', DB::raw('ROUND(AVG(nota), 2) as nota_resultado')])
            ->groupBy('casos_clinicos.id')
            ->orderBy('nota_resultado', 'DESC')
            ->limit($amount)
            ->get()
            ->map(function($caso) {
                $caso->nota_resultado = CasoClinicoAvaliacao::formataNota($caso->nota_resultado);
                return $caso;
            });
    }
}
