<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Grupo extends Model
{
    protected $table = 'grupos';

    protected $fillable = [
        'data_limite',
    ];

    protected $casts = [
        'data_limite' => 'datetime:d/m/y',
    ];

    public function setDataLimiteAttribute($date)
    {
        $this->attributes['data_limite'] = $date
            ? Carbon::createFromFormat('d/m/y', $date)->format('Y-m-d')
            : null;
    }

    public function medicos()
    {
        return $this->hasMany(Medico::class, 'grupo_id');
    }
}
