<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResetPassword extends Notification
{
    public function __construct($route, $token)
    {
        $this->route = $route;
        $this->token = $token;
    }

    public function via()
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $url = url(route($this->route, [
            'token' => $this->token,
            'email' => $notifiable->getEmailForPasswordReset(),
        ], false));

        return (new MailMessage)
            ->subject('Solicitação de redefinição de senha')
            ->greeting('Olá!')
            ->line('Você está recebendo este e-mail porque solicitou a redefinição de senha da sua conta.')
            ->line('Para redefinir sua senha clique no botão abaixo.')
            ->action('Redefinir senha', $url)
            ->line('Se você não realizou essa solicitação, ignore este e-mail.');
    }
}
