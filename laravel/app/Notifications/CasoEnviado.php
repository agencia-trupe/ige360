<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CasoEnviado extends Notification
{
    public function __construct($casoClinico)
    {
        $this->casoClinico = $casoClinico;
    }

    public function via()
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $url = route('farmacovigilancia.casos-clinicos', $this->casoClinico->id);

        return (new MailMessage)
            ->subject('Há um novo Caso Clínico para revisão')
            ->greeting("Prezado(a) $notifiable->nome,")
            ->line('Há um novo Caso Clínico para sua revisão na plataforma IgE 360.')
            ->line('Postagem no sistema: '.$this->casoClinico->enviado_em->format('d/m/y - H:i \\h'))
            ->action('Visualizar Caso Clínico', $url);
    }
}
