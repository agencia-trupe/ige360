<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class CasoValidado extends Notification
{
    public function __construct($casoClinico)
    {
        $this->casoClinico = $casoClinico;
    }

    public function via()
    {
        return ['mail'];
    }

    public function toMail()
    {
        $url = route('administracao.casos-clinicos');

        return (new MailMessage)
            ->subject('Há um novo Caso Clínico liberado pela Farmacovigilância')
            ->greeting('Prezado(a) Administrador(a) IgE 360,')
            ->line('Um novo Caso Clínico foi liberado pela Farmacovigilância para ser distribuído aos avaliadores.')
            ->line(new HtmlString('<strong>'.$this->casoClinico->titulo.' - '.$this->casoClinico->enviado_em->format('d/m/y - H:i \\h').'</strong>'))
            ->line('Acesse a plataforma para encaminhar o Caso aos Avaliadores.')
            ->action('Acessar Plataforma', $url);
    }
}
