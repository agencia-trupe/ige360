<?php

namespace App\Notifications;

use App\Models\TextoConvite;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class ConviteMedico extends Notification
{
    public function via()
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $textoConvite = TextoConvite::first();
        $dataLimite   = $notifiable->grupo->data_limite->format('d/m/y');

        $url = url(route('medico.register', [
            'token' => $notifiable->token_convite,
            'email' => $notifiable->email
        ], false));

        return (new MailMessage)
            ->subject('Você foi convidado para o concurso de casos clínicos!')
            ->greeting('Olá!')
            ->line(new HtmlString($textoConvite->texto))
            ->line('Para acessar o sistema pela primeira vez e criar sua senha clique no botão abaixo.')
            ->action('Acessar sistema', $url)
            ->line(new HtmlString('Seu prazo de submissão do Caso Clínico é: <strong>'.$dataLimite.'</strong>'));
    }
}
