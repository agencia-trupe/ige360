<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CasoDistribuido extends Notification
{
    public function __construct($casoClinico)
    {
        $this->casoClinico = $casoClinico;
    }

    public function via()
    {
        return ['mail'];
    }

    public function toMail()
    {
        $url = route('avaliador.casos-clinicos', $this->casoClinico->id);

        return (new MailMessage)
            ->subject('Há um novo Caso Clínico para avaliação')
            ->greeting('Prezado(a) Avaliador(a),')
            ->line('Há um novo Caso Clínico para sua avaliação na plataforma IgE 360.')
            ->line('Postagem no sistema: '.$this->casoClinico->enviado_em->format('d/m/y - H:i \\h'))
            ->action('Avaliar Caso Clínico', $url);
    }
}
