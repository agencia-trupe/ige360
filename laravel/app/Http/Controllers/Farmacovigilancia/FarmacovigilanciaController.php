<?php

namespace App\Http\Controllers\Farmacovigilancia;

use App\Http\Controllers\Controller;

class FarmacovigilanciaController extends Controller
{
    public function index()
    {
        return redirect()->route('farmacovigilancia.casos-clinicos');
    }
}
