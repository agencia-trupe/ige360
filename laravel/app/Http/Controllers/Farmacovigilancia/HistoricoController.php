<?php

namespace App\Http\Controllers\Farmacovigilancia;

use App\Http\Controllers\Controller;

use App\Models\CasoClinico;

class HistoricoController extends Controller
{
    public function index()
    {
        $user = auth('farmacovigilancia')->user();

        return view('farmacovigilancia.historico.index', [
            'casosClinicos' => CasoClinico::validado($user->id)->get()
        ]);
    }
}
