<?php

namespace App\Http\Controllers\Farmacovigilancia\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    protected function guard()
    {
        return Auth::guard('farmacovigilancia');
    }

    public function broker()
    {
        return Password::broker('farmacovigilantes');
    }

    public function showLinkRequestForm()
    {
        return view('farmacovigilancia.auth.passwords.email');
    }
}
