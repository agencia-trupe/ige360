<?php

namespace App\Http\Controllers\Farmacovigilancia\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    protected $redirectTo = '/farmacovigilancia';

    public function broker()
    {
        return Password::broker('farmacovigilantes');
    }

    protected function guard()
    {
        return Auth::guard('farmacovigilancia');
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('farmacovigilancia.auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}
