<?php

namespace App\Http\Controllers\Farmacovigilancia\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/farmacovigilancia/casos-clinicos';

    protected function guard()
    {
        return Auth::guard('farmacovigilancia');
    }

    public function showLoginForm()
    {
        return view('farmacovigilancia.auth.login');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/farmacovigilancia/login');
    }
}
