<?php

namespace App\Http\Controllers\Farmacovigilancia;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\CasoClinico;
use App\Models\Administrador;
use App\Notifications\CasoValidado;

class CasosClinicosController extends Controller
{
    public function index($id = null)
    {
        if ($id) {
            return view('farmacovigilancia.casos-clinicos.edit', [
                'casoClinico' => CasoClinico::paraValidar()->findOrFail($id)
            ]);
        }

        return view('farmacovigilancia.casos-clinicos.index', [
            'casosClinicos' => CasoClinico::paraValidar()->get()
        ]);
    }

    public function post(Request $request, $id)
    {
        $casoClinico = CasoClinico::paraValidar()->findOrFail($id);

        $this->validateCaso($request);

        try {
            $this->saveCaso($casoClinico, $request);

            return back()->with('success', 'Caso clínico salvo com sucesso!');
        } catch (\Exception $e) {
            return back()
                ->withInput()
                ->withErrors(['Ocorreu um erro ao salvar o caso clínico.']);
        }
    }

    public function validacao(Request $request, $id)
    {
        $casoClinico = CasoClinico::paraValidar()->findOrFail($id);

        $this->validateCaso($request);

        try {
            $this->saveCaso($casoClinico, $request, true);

            return redirect()
                ->route('farmacovigilancia.casos-clinicos')
                ->with('success', 'Caso clínico validado com sucesso!');
        } catch (\Exception $e) {
            return back()
                ->withInput()
                ->withErrors(['Ocorreu um erro ao validar o caso clínico.']);
        }
    }

    protected function validateCaso(Request $request)
    {
        $request->validate([
            'titulo'    => 'required',
            'relato'    => 'required',
            'conclusao' => 'required',
        ]);
    }

    protected function saveCaso($casoClinico, Request $request, $validar = false)
    {
        $farmacovigilante = auth('farmacovigilancia')->user();

        $casoClinico->update([
            'titulo'       => $request->titulo,
            'relato'       => $request->relato,
            'conclusao'    => $request->conclusao,
            'validado_em'  => $validar ? Carbon::now() : null,
            'validado_por' => $validar ? $farmacovigilante->id : null,
        ]);

        if ($validar) {
            Notification::send(
                Administrador::all(),
                new CasoValidado($casoClinico)
            );
        }
    }
}
