<?php

namespace App\Http\Controllers\Avaliador;

use App\Http\Controllers\Controller;

class AvaliadorController extends Controller
{
    public function index()
    {
        return redirect()->route('avaliador.casos-clinicos');
    }
}
