<?php

namespace App\Http\Controllers\Avaliador;

use App\Http\Controllers\Controller;

use App\Models\CasoClinico;

class HistoricoController extends Controller
{
    public function index()
    {
        $avaliador = auth('avaliador')->user();

        return view('avaliador.historico.index', [
            'casosClinicos' => CasoClinico::avaliado($avaliador->id)
                ->get()
                ->map(function ($casoClinico) use ($avaliador) {
                    $casoClinico->avaliado_em = $casoClinico->avaliacoes->first(function($avaliacao) use ($avaliador) {
                        return $avaliacao->avaliador_id = $avaliador->id;
                    })->created_at;

                    return $casoClinico;
                })
        ]);
    }
}
