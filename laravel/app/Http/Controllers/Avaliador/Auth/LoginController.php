<?php

namespace App\Http\Controllers\Avaliador\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/avaliador/casos-clinicos';

    protected function guard()
    {
        return Auth::guard('avaliador');
    }

    public function showLoginForm()
    {
        return view('avaliador.auth.login');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/avaliador/login');
    }
}
