<?php

namespace App\Http\Controllers\Avaliador\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    protected $redirectTo = '/avaliador';

    public function broker()
    {
        return Password::broker('avaliadores');
    }

    protected function guard()
    {
        return Auth::guard('avaliador');
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('avaliador.auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}
