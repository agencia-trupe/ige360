<?php

namespace App\Http\Controllers\Avaliador\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    protected function guard()
    {
        return Auth::guard('avaliador');
    }

    public function broker()
    {
        return Password::broker('avaliadores');
    }

    public function showLinkRequestForm()
    {
        return view('avaliador.auth.passwords.email');
    }
}
