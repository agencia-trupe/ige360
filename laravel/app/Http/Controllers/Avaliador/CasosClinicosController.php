<?php

namespace App\Http\Controllers\Avaliador;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\CasoClinico;

class CasosClinicosController extends Controller
{
    public function index($id = null)
    {
        $avaliador = auth('avaliador')->user();

        if ($id) {
            return view('avaliador.casos-clinicos.edit', [
                'casoClinico' => CasoClinico::paraAvaliar($avaliador->id)->findOrFail($id)
            ]);
        }

        return view('avaliador.casos-clinicos.index', [
            'casosClinicos' => CasoClinico::paraAvaliar($avaliador->id)->get()
        ]);
    }

    public function post(Request $request, $id)
    {
        $avaliador   = auth('avaliador')->user();
        $casoClinico = CasoClinico::paraAvaliar($avaliador->id)->findOrFail($id);

        $request->merge(['nota' => str_replace(',', '.', $request->nota)]);
        $request->validate(['nota' => 'required|numeric|between:0,10']);

        try {
            $casoClinico->avaliacoes()->create([
                'caso_id'      => $casoClinico->id,
                'avaliador_id' => $avaliador->id,
                'nota'         => $request->nota,
            ]);

            return redirect()
                ->route('avaliador.casos-clinicos')
                ->with('success', 'Caso clínico avaliado com sucesso!');
        } catch (\Exception $e) {
            return back()
                ->withInput()
                ->withErrors(['Ocorreu um erro ao avaliar o caso clínico.']);
        }
    }
}
