<?php

namespace App\Http\Controllers\Medico;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Carbon\Carbon;

use App\Models\Farmacovigilante;
use App\Models\CasoClinico;
use App\Notifications\CasoEnviado;

class CasoClinicoController extends Controller
{
    public function index()
    {
        $medico      = auth('medico')->user();
        $casoClinico = $medico->casoClinico;

        if (env('EXIBIR_RESULTADO')) {
            return view('medico.caso-clinico.resultado', [
                'vencedor'    => CasoClinico::resultadosOrdenados(1)->first(),
                'casoClinico' => $casoClinico,
            ]);
        }

        if ($casoClinico && $casoClinico->enviado_em) {
            return view('medico.caso-clinico.enviado', compact('casoClinico'));
        }

        return view('medico.caso-clinico.index', compact('medico', 'casoClinico'));
    }

    public function post(Request $request)
    {
        $this->validateCaso($request);

        try {
            $this->saveCaso($request);

            return back()->with('success', 'Caso clínico salvo com sucesso!');
        } catch (\Exception $e) {
            return back()
                ->withInput()
                ->withErrors(['Ocorreu um erro ao salvar o caso clínico.']);
        }
    }

    public function submissao(Request $request)
    {
        $this->validateCaso($request);

        try {
            $this->saveCaso($request, true);

            return back();
        } catch (\Exception $e) {
            return back()
                ->withInput()
                ->withErrors(['Ocorreu um erro ao submeter o caso clínico.']);
        }
    }

    protected function validateCaso(Request $request)
    {
        $request->validate([
            'nome'                       => 'required',
            'crm'                        => 'required',
            'telefone'                   => 'required',
            'participantes'              => 'required|array',
            'titulo'                     => 'required',
            'relato'                     => 'required',
            'conclusao'                  => 'required',
            'referencias_bibliograficas' => 'required'
        ]);
    }

    protected function saveCaso(Request $request, $enviar = false)
    {
        $medico = auth('medico')->user();

        if ($enviar && !$medico->podeSubmeterCasoClinico) {
            throw new \Exception('Prazo de submissão expirado.');
        }

        $medico->update([
            'nome'     => $request->nome,
            'crm'      => $request->crm,
            'telefone' => $request->telefone,
        ]);

        $medico->casoClinico()->updateOrCreate(
            ['medico_id' => $medico->id],
            [
                'participantes'              => $request->participantes,
                'titulo'                     => $request->titulo,
                'relato'                     => $request->relato,
                'conclusao'                  => $request->conclusao,
                'referencias_bibliograficas' => $request->referencias_bibliograficas,
                'enviado_em'                 => $enviar ? Carbon::now() : null,
            ]
        );

        if ($enviar) {
            Notification::send(
                Farmacovigilante::all(),
                new CasoEnviado($medico->casoClinico)
            );
        }
    }
}
