<?php

namespace App\Http\Controllers\Medico;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RegulamentoController extends Controller
{
    public function index()
    {
        return view('medico.regulamento.index');
    }

    public function post(Request $request)
    {
        if (!$request->consentimento) {
            return back()->withErrors(['A participação no do Concurso de Casos Clínicos IgE 360 da Novartis depende do tratamento de seus dados pessoais.']);
        }

        $user = auth('medico')->user();

        if (!$user->termos_aceitos_em) {
            $user->update(['termos_aceitos_em' => Carbon::now()]);
        }

        return redirect()->route('medico.cronograma');
    }
}
