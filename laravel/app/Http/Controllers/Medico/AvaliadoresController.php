<?php

namespace App\Http\Controllers\Medico;

use App\Http\Controllers\Controller;

class AvaliadoresController extends Controller
{
    public function index()
    {
        return view('medico.avaliadores.index');
    }
}
