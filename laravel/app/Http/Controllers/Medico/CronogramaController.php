<?php

namespace App\Http\Controllers\Medico;

use App\Http\Controllers\Controller;

class CronogramaController extends Controller
{
    public function index()
    {
        return view('medico.cronograma.index');
    }
}
