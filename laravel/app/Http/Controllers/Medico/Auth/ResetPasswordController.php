<?php

namespace App\Http\Controllers\Medico\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    protected $redirectTo = '/';

    protected function guard()
    {
        return Auth::guard('medico');
    }

    public function broker()
    {
        return Password::broker('medicos');
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('medico.auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}
