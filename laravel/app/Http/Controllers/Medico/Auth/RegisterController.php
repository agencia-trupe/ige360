<?php

namespace App\Http\Controllers\Medico\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Models\Medico;
use Carbon\Carbon;

class RegisterController extends Controller
{
    protected $redirectTo = '/regulamento';

    public function index()
    {
        return view('medico.auth.register');
    }

    public function post(Request $request)
    {
        $request->validate([
            'token'    => 'required',
            'email'    => 'required|email',
            'password' => 'required|confirmed|min:8',
        ]);

        $medico = Medico::where([
            'email'         => $request->email,
            'token_convite' => $request->token,
        ])->first();

        if (!$medico) {
            return back()->withErrors(['Token inválido.']);
        }

        $medico->update([
            'password'      => Hash::make($request->password),
            'token_convite' => null,
            'cadastrado_em' => Carbon::now(),
        ]);

        auth('medico')->login($medico);

        return redirect($this->redirectTo);
    }
}
