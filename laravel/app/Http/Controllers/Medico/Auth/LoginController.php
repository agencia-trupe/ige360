<?php

namespace App\Http\Controllers\Medico\Auth;

use App\Http\Controllers\Controller;
use App\Models\Medico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/regulamento';

    protected function guard()
    {
        return Auth::guard('medico');
    }

    public function showLoginForm()
    {
        return view('medico.auth.login');
    }

    public function login(Request $request)
    {
        $medico = Medico::where('email', $request->email)->first();

        if ($medico && !$medico->cadastrado_em) {
            return $this->sendFailedLoginResponse($request);
        }

        $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
