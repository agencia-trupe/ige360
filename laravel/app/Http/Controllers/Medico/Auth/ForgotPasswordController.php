<?php

namespace App\Http\Controllers\Medico\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

use App\Models\Medico;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    protected function guard()
    {
        return Auth::guard('medico');
    }

    public function broker()
    {
        return Password::broker('medicos');
    }

    public function showLinkRequestForm()
    {
        return view('medico.auth.passwords.email');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        $medico = Medico::where('email', $request->email)->first();

        if ($medico && !$medico->cadastrado_em) {
            return $this->sendResetLinkFailedResponse($request, 'E-mail inválido.');
        }

        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        );

        return $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($request, $response)
                    : $this->sendResetLinkFailedResponse($request, $response);
    }
}
