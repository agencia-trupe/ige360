<?php

namespace App\Http\Controllers\Medico;

use App\Http\Controllers\Controller;

class MedicoController extends Controller
{
    public function index()
    {
        return redirect()->route('medico.regulamento');
    }
}
