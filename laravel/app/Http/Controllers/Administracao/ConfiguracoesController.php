<?php

namespace App\Http\Controllers\Administracao;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Models\Grupo;
use App\Models\Avaliador;
use App\Models\Farmacovigilante;
use App\Models\Administrador;

class ConfiguracoesController extends Controller
{
    public function index()
    {
        return view('administracao.configuracoes.index', [
            'grupos'            => Grupo::all(),
            'avaliadores'       => Avaliador::all(),
            'farmacovigilantes' => Farmacovigilante::all(),
            'administradores'   => Administrador::all(),
        ]);
    }


    private function createOrUpdate($input, $class) {
        $data = [];

        if (!is_array($input)) {
            return;
        }

        foreach(array_keys($input) as $fieldKey) {
            foreach($input[$fieldKey] as $key => $value) {
                $data[$key][$fieldKey] = $value;
            }
        }

        foreach($data as $key => $obj) {
            $newObj = array_merge(
                array_diff_assoc($obj, ['id' => $obj['id']]),
                in_array($class, [Farmacovigilante::class, Administrador:: class])
                    ? ['password' => Hash::make(Str::random(32))]
                    : []
            );

            $obj['id'] === 'create'
                ? $class::create($newObj)
                : $class::findOrFail($obj['id'])->update($obj);
        }
    }

    public function post(Request $request)
    {
        try {
            $this->createOrUpdate($request->grupo, Grupo::class);
            $this->createOrUpdate($request->avaliador, Avaliador::class);
            $this->createOrUpdate($request->farmacovigilante, Farmacovigilante::class);
            $this->createOrUpdate($request->administrador, Administrador::class);

            return back()->with('success', 'Dados atualizados com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Ocorreu um erro ao atualizar os dados.']);
        }
    }

    public function delete(Request $request)
    {
        try {
            if ($request->tipo === 'farmacovigilante') {
                $obj = Farmacovigilante::findOrFail($request->id);
            } else if ($request->tipo === 'administrador') {
                $obj = Administrador::findOrFail($request->id);
            }

            if (!$obj || ($request->tipo === 'administrador' && $obj->id === auth('administracao')->user()->id)) {
                abort('500');
            }

            $obj->delete();

            return back()->with('success', 'Usuário excluído com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Ocorreu um erro ao excluir o usuário.']);
        }
    }
}
