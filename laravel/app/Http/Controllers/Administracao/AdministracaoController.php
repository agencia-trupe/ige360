<?php

namespace App\Http\Controllers\Administracao;

use App\Http\Controllers\Controller;

class AdministracaoController extends Controller
{
    public function index()
    {
        return redirect()->route('administracao.coordenadores');
    }
}
