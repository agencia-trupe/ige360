<?php

namespace App\Http\Controllers\Administracao;

use App\Http\Controllers\Controller;

use App\Models\CasoClinico;

class ResultadosController extends Controller
{
    public function index()
    {
        return view('administracao.resultados.index', [
            'resultados' => CasoClinico::resultadosOrdenados(3)
        ]);
    }
}
