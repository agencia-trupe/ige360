<?php

namespace App\Http\Controllers\Administracao\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/administracao/coordenadores';

    protected function guard()
    {
        return Auth::guard('administracao');
    }

    public function showLoginForm()
    {
        return view('administracao.auth.login');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/administracao/login');
    }
}
