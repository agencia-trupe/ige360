<?php

namespace App\Http\Controllers\Administracao\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    protected function guard()
    {
        return Auth::guard('administracao');
    }

    public function broker()
    {
        return Password::broker('administradores');
    }

    public function showLinkRequestForm()
    {
        return view('administracao.auth.passwords.email');
    }
}
