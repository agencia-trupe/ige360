<?php

namespace App\Http\Controllers\Administracao\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    protected $redirectTo = '/administracao';

    public function broker()
    {
        return Password::broker('administradores');
    }

    protected function guard()
    {
        return Auth::guard('administracao');
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('administracao.auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
}
