<?php

namespace App\Http\Controllers\Administracao;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Carbon\Carbon;

use App\Models\Avaliador;
use App\Models\CasoClinico;
use App\Notifications\CasoDistribuido;

class CasosClinicosController extends Controller
{
    protected $avaliadores;

    public function __construct()
    {
        $this->avaliadores = Avaliador::get();
    }

    public function index(Request $request)
    {
        $casosClinicos = CasoClinico::with('medico', 'avaliacoes')
            ->withCount('avaliacoes')
            ->validado();

        if ($request->filtro === 'sem_avaliacoes') {
            $casosClinicos = $casosClinicos->doesntHave('avaliacoes');
        } else if ($request->filtro === 'avaliados') {
            $casosClinicos = $casosClinicos->has('avaliacoes');
        }

        if ($request->distribuido === 'sim') {
            $casosClinicos = $casosClinicos->whereNotNull('distribuido_em');
        } else if ($request->distribuido === 'nao') {
            $casosClinicos = $casosClinicos->whereNull('distribuido_em');
        }

        if ($termo = $request->busca) {
            $casosClinicos = $casosClinicos->whereHas('medico', function($query) use ($termo) {
                $query
                    ->where('nome', 'LIKE', "%$termo%")
                    ->orWhere('email', 'LIKE', "%$termo%");
            });
        }

        return view('administracao.casos-clinicos.index', [
            'avaliadoresCount' => $this->avaliadores->count(),
            'casosClinicos'    => $casosClinicos->get()->map(function($casoClinico) {
                return $this->spreadAvaliacoes($casoClinico);
            })
        ]);
    }

    public function show($id)
    {
        $casoClinico = CasoClinico::validado()->findOrFail($id);

        if ($casoClinico->distribuido_em) {
            return view('administracao.casos-clinicos.show', [
                'casoClinico' => $this->spreadAvaliacoes($casoClinico)
            ]);
        }

        return view('administracao.casos-clinicos.edit', compact('casoClinico'));
    }

    public function post(Request $request, $id)
    {
        $casoClinico = CasoClinico::paraDistribuir()->findOrFail($id);

        $this->validateCaso($request);

        try {
            $this->saveCaso($casoClinico, $request);

            return back()->with('success', 'Caso clínico salvo com sucesso!');
        } catch (\Exception $e) {
            return back()
                ->withInput()
                ->withErrors(['Ocorreu um erro ao salvar o caso clínico.']);
        }
    }

    public function distribuir(Request $request, $id)
    {
        $casoClinico = CasoClinico::paraDistribuir()->findOrFail($id);

        $this->validateCaso($request);

        try {
            $this->saveCaso($casoClinico, $request, true);

            return redirect()
                ->route('administracao.casos-clinicos')
                ->with('success', 'Caso clínico distribuído com sucesso!');
        } catch (\Exception $e) {
            return back()
                ->withInput()
                ->withErrors(['Ocorreu um erro ao distribuir o caso clínico.']);
        }
    }

    protected function spreadAvaliacoes(CasoClinico $casoClinico)
    {
        $casoClinico->avaliacoes = $this->avaliadores->map(function($avaliador) use ($casoClinico) {
            return $casoClinico->avaliacoes->first(function($avaliacao) use ($avaliador) {
                return $avaliacao->avaliador_id === $avaliador->id;
            });
        });

        return $casoClinico;
    }

    protected function validateCaso(Request $request)
    {
        $request->validate([
            'nome'                       => 'required',
            'crm'                        => 'required',
            'telefone'                   => 'required',
            'participantes'              => 'required|array',
            'titulo'                     => 'required',
            'relato'                     => 'required',
            'conclusao'                  => 'required',
            'referencias_bibliograficas' => 'required'
        ]);
    }

    protected function saveCaso($casoClinico, Request $request, $distribuir = false)
    {
        $administrador   = auth('administracao')->user();
        $casoClinicoCopy = $casoClinico->replicate();

        $medico = $casoClinico->medico;
        $medico->update([
            'nome'     => $request->nome,
            'crm'      => $request->crm,
            'telefone' => $request->telefone,
        ]);

        $casoClinico->update([
            'participantes'              => $request->participantes,
            'titulo'                     => $request->titulo,
            'relato'                     => $request->relato,
            'conclusao'                  => $request->conclusao,
            'referencias_bibliograficas' => $request->referencias_bibliograficas,
            'distribuido_em'             => $distribuir ? Carbon::now() : null,
            'distribuido_por'            => $distribuir ? $administrador->id : null,
        ]);

        /**
         * Verifica alterações nos dados do caso clínico / médico
         * Se houverem mudanças grava a data e id do administrador
         * em um json na coluna "alteracoes_administracao"
         */

        $casoClinicoChanges = array_filter($casoClinico->getChanges(), function ($key) {
            return !in_array($key, ['participantes', 'updated_at']);
        }, ARRAY_FILTER_USE_KEY);

        $previousParticipantes = json_decode($casoClinicoCopy->getAttributes()['participantes'], true);
        $participantesChanges  = array_filter($request->participantes, function($participante, $index) use ($previousParticipantes) {
            return
                $participante['nome'] !== $previousParticipantes[$index]['nome'] ||
                $participante['crm'] !== $previousParticipantes[$index]['crm'];
        }, ARRAY_FILTER_USE_BOTH);

        if (count($casoClinicoChanges) || count($participantesChanges) || count($medico->getChanges())) {
            $alteracoes   = json_decode($casoClinico->alteracoes_administracao, true) ?: [];
            $alteracoes[] = [
                'administrador_id' => $administrador->id,
                'data_alteracao'   => Carbon::now()
            ];

            $casoClinico->update(['alteracoes_administracao' => json_encode($alteracoes)]);
        }

        if ($distribuir) {
            Notification::send(
                Avaliador::all(),
                new CasoDistribuido($casoClinico)
            );
        }
    }
}
