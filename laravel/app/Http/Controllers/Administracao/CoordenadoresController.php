<?php

namespace App\Http\Controllers\Administracao;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

use App\Models\Grupo;
use App\Models\Medico;
use App\Models\TextoConvite;
use App\Notifications\ConviteMedico;

class CoordenadoresController extends Controller
{
    public function index(Request $request)
    {
        if ($request->busca) {
            return view('administracao.coordenadores.busca', [
                'medicos' => Medico::busca($request->busca)->get()
            ]);
        }

        return view('administracao.coordenadores.index', [
            'grupos' => Grupo::all()
        ]);
    }

    public function grupo(Request $request, $id)
    {
        $grupo = Grupo::findOrFail($id);

        return view('administracao.coordenadores.grupo', [
            'grupo'   => $grupo,
            'medicos' => $request->busca
                ? $grupo->medicos()->busca($request->busca)->get()
                : $grupo->medicos
        ]);
    }

    public function grupoPost(Request $request, $id)
    {
        $grupo = Grupo::findOrFail($id);

        $request->validate([
            'nome' => 'required',
            'email' => 'required|email|unique:medicos,email',
        ]);

        try {
            $grupo->medicos()->create([
                'nome'          => $request->nome,
                'email'         => $request->email,
                'password'      => Hash::make(Str::random(32)),
                'token_convite' => Str::random(32)
            ]);

            return back()->with('success', 'Coordenador cadastrado com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Ocorreu um erro ao cadastrar o coordenador.']);
        }
    }

    public function grupoDelete($grupoId, $medicoId)
    {
        try {
            $grupo  = Grupo::findOrFail($grupoId);
            $medico = Medico::where('grupo_id', $grupo->id)->findOrFail($medicoId);

            if ($medico->casoClinico && $medico->casoClinico->enviado_em) {
                return back()->withErrors(['Não é possível excluir um coordenador que já enviou um caso clínico']);
            }

            $medico->delete();

            return back()->with('success', 'Coordenador excluído com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Ocorreu um erro ao excluir o coordenador.']);
        }
    }

    public function grupoConvite($grupoId, $medicoId)
    {
        try {
            $grupo  = Grupo::findOrFail($grupoId);
            $medico = Medico::where('grupo_id', $grupo->id)->findOrFail($medicoId);
            $field  = $medico->convite_enviado_em ? 'convite_reenviado_em': 'convite_enviado_em';

            if ($medico->cadastrado_em) {
                return back()->withErrors(['Médico já está cadastrado.']);
            }

            if (!$grupo->data_limite) {
                return back()->withErrors(['Cadastre a data limite do grupo antes de enviar convites.']);
            }

            $medico->update([
                $field          =>  Carbon::now(),
                'token_convite' => Str::random(32)
            ]);
            $medico->notify(new ConviteMedico($medico));

            return back()->with('success', 'Convite enviado com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Ocorreu um erro ao enviar o convite.']);
        }

    }

    public function textoConvite()
    {
        return view('administracao.coordenadores.texto-convite', [
            'textoConvite' => TextoConvite::first()
        ]);
    }

    public function textoConvitePost(Request $request)
    {
        $request->validate([
            'texto' => 'required'
        ]);

        try {
            $texto = TextoConvite::firstOrFail();
            $texto->update([
                'texto' => $request->texto
            ]);

            return back()->with('success', 'Texto salvo com sucesso!');
        } catch (\Exception $e) {
            return back()->withErrors(['Ocorreu um erro ao salvar o texto.']);
        }
    }
}
