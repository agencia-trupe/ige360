<?php

namespace App\Http\Middleware\Farmacovigilancia;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    public function handle($request, Closure $next, $guard = 'farmacovigilancia')
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/farmacovigilancia');
        }

        return $next($request);
    }
}
