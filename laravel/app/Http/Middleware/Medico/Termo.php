<?php

namespace App\Http\Middleware\Medico;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Termo extends Middleware
{
    public function handle($request, Closure $next)
    {
        if (!auth('medico')->user()->termos_aceitos_em) {
            return redirect()
                ->route('medico.regulamento')
                ->withErrors(['É necessário aceitar o termo antes de continuar.']);
        }

        return $next($request);
    }
}
