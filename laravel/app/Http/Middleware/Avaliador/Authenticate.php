<?php

namespace App\Http\Middleware\Avaliador;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    public function handle($request, Closure $next)
    {
        $this->authenticate($request, ['avaliador']);

        return $next($request);
    }

    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('avaliador.login');
        }
    }
}
