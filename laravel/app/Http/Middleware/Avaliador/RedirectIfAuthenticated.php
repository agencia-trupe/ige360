<?php

namespace App\Http\Middleware\Avaliador;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    public function handle($request, Closure $next, $guard = 'avaliador')
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/avaliador');
        }

        return $next($request);
    }
}
