<?php

namespace App\Http\Middleware\Administracao;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    public function handle($request, Closure $next, $guard = 'administracao')
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/administracao');
        }

        return $next($request);
    }
}
