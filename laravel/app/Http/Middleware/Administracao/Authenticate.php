<?php

namespace App\Http\Middleware\Administracao;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    public function handle($request, Closure $next)
    {
        $this->authenticate($request, ['administracao']);

        return $next($request);
    }

    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            return route('administracao.login');
        }
    }
}
