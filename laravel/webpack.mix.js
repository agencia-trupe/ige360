const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix
    .setPublicPath('../public')
    .options({
        terser: { extractComments: false },
        processCssUrls: false
    })
    .js('resources/js/main.js', 'assets/js')
    .stylus('resources/stylus/main.styl', 'assets/css', {
        use: [require('rupture')()],
    })
    .browserSync('localhost:8000');

mix.inProduction() && mix.version();
