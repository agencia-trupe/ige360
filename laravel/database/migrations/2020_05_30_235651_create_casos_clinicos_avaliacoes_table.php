<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCasosClinicosAvaliacoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casos_clinicos_avaliacoes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('caso_id')->references('id')->on('casos_clinicos');
            $table->foreignId('avaliador_id')->references('id')->on('avaliadores');
            $table->unique(['caso_id', 'avaliador_id']);
            $table->decimal('nota', 4, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casos_clinicos_avaliacoes');
    }
}
