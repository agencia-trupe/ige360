<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCasosClinicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casos_clinicos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('medico_id')->unique()->references('id')->on('medicos');
            $table->json('participantes')->nullable();
            $table->string('titulo');
            $table->text('relato');
            $table->text('conclusao');
            $table->text('referencias_bibliograficas');
            $table->dateTime('enviado_em')->nullable();
            $table->dateTime('validado_em')->nullable();
            $table->foreignId('validado_por')->nullable()->references('id')->on('farmacovigilantes');
            $table->dateTime('distribuido_em')->nullable();
            $table->foreignId('distribuido_por')->nullable()->references('id')->on('administradores');
            $table->json('alteracoes_administracao')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casos_clinicos');
    }
}
