<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('grupo_id')->references('id')->on('grupos');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('nome');
            $table->string('crm')->nullable();
            $table->string('telefone')->nullable();
            $table->string('token_convite')->nullable();
            $table->dateTime('convite_enviado_em')->nullable();
            $table->dateTime('convite_reenviado_em')->nullable();
            $table->dateTime('cadastrado_em')->nullable();
            $table->dateTime('termos_aceitos_em')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicos');
    }
}
