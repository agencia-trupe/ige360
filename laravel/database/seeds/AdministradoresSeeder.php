<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdministradoresSeeder extends Seeder
{
    public function run()
    {
        DB::table('administradores')->insert([
            'email'    => 'admin@trupe.net',
            'password' => Hash::make('senhatrupe'),
            'nome'     => 'Trupe',
        ]);
    }
}
