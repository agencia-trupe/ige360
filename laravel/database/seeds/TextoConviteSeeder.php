<?php

use Illuminate\Database\Seeder;

class TextoConviteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('texto_convite')->insert([
            'texto' => '',
        ]);
    }
}
