<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdministradoresSeeder::class);
        $this->call(AvaliadoresSeeder::class);
        $this->call(GruposSeeder::class);
        $this->call(TextoConviteSeeder::class);
    }
}
