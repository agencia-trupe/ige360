<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AvaliadoresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];

        foreach(range(1, 6) as $i) {
            $data[] = [
                'nome'     => 'Avaliador '.$i,
                'email'    => 'avaliador'.$i.'@ige360.com.br',
                'password' => Hash::make(Str::random(32))
            ];
        }

        DB::table('avaliadores')->insert($data);
    }
}
