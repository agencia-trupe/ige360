<?php

use Illuminate\Support\Facades\Route;

Route::namespace('Administracao')
    ->prefix('administracao')
    ->name('administracao.')
    ->group(function() {
        Route::namespace('Auth')
            ->middleware(['administracao.guest'])
            ->group(function() {
                Route::get('login', 'LoginController@showLoginForm')
                    ->name('login');
                Route::post('login', 'LoginController@login')
                    ->name('login.post');
                Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')
                    ->name('password.request');
                Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')
                    ->name('password.email');
                Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')
                    ->name('password.reset');
                Route::post('password/reset', 'ResetPasswordController@reset')
                    ->name('password.update');
            });

        Route::middleware(['administracao.auth'])
            ->group(function() {
                Route::get('/', 'AdministracaoController@index')
                    ->name('home');

                Route::get('coordenadores', 'CoordenadoresController@index')
                    ->name('coordenadores');
                Route::get('coordenadores/grupo/{id}', 'CoordenadoresController@grupo')
                    ->name('coordenadores.grupo');
                Route::post('coordenadores/grupo/{id}', 'CoordenadoresController@grupoPost')
                    ->name('coordenadores.grupo.post');
                Route::get('coordenadores/grupo/{id}/delete/{medico_id}', 'CoordenadoresController@grupoDelete')
                    ->name('coordenadores.grupo.delete');
                Route::get('coordenadores/grupo/{id}/convite/{medico_id}', 'CoordenadoresController@grupoConvite')
                    ->name('coordenadores.grupo.convite');
                Route::get('coordenadores/texto-convite', 'CoordenadoresController@textoConvite')
                    ->name('coordenadores.texto-convite');
                Route::post('coordenadores/texto-convite', 'CoordenadoresController@textoConvitePost')
                    ->name('coordenadores.texto-convite.post');

                Route::get('casos-clinicos', 'CasosClinicosController@index')
                    ->name('casos-clinicos');
                Route::get('casos-clinicos/{id}', 'CasosClinicosController@show')
                    ->name('casos-clinicos.show');
                Route::post('casos-clinicos/{id}', 'CasosClinicosController@post')
                    ->name('casos-clinicos.post');
                Route::post('casos-clinicos/{id}/distribuir', 'CasosClinicosController@distribuir')
                    ->name('casos-clinicos.distribuir');

                Route::get('resultados', 'ResultadosController@index')
                    ->name('resultados');

                Route::get('configuracoes', 'ConfiguracoesController@index')
                    ->name('configuracoes');
                Route::post('configuracoes', 'ConfiguracoesController@post')
                    ->name('configuracoes.post');
                Route::get(
                    'configuracoes/delete/{tipo}/{id}',
                    'ConfiguracoesController@delete'
                )->name('configuracoes.delete');

                Route::get('logout', 'Auth\LoginController@logout')
                    ->name('logout');
            });
    });
