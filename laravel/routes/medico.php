<?php

use Illuminate\Support\Facades\Route;

Route::get('aviso-de-privacidade', function() {
    return view('aviso-de-privacidade');
})->name('aviso-de-privacidade');

Route::namespace('Medico')
    ->name('medico.')
    ->group(function() {
        Route::namespace('Auth')
            ->middleware(['medico.guest'])
            ->group(function() {
                Route::get('login', 'LoginController@showLoginForm')
                    ->name('login');
                Route::post('login', 'LoginController@login')
                    ->name('login.post');
                Route::get('cadastro', 'RegisterController@index')
                    ->name('register');
                Route::post('cadastro', 'RegisterController@post')
                    ->name('register.post');
                Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')
                    ->name('password.request');
                Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')
                    ->name('password.email');
                Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')
                    ->name('password.reset');
                Route::post('password/reset', 'ResetPasswordController@reset')
                    ->name('password.update');
            });

        Route::middleware(['medico.auth'])
            ->group(function() {
                Route::get('/', 'MedicoController@index')
                    ->name('home');

                Route::get('regulamento', 'RegulamentoController@index')
                    ->name('regulamento');
                Route::post('regulamento', 'RegulamentoController@post')
                    ->name('regulamento.post');

                Route::middleware(['medico.termo'])
                    ->group(function() {
                        Route::get('cronograma', 'CronogramaController@index')
                            ->name('cronograma');

                        Route::get('avaliadores', 'AvaliadoresController@index')
                            ->name('avaliadores');

                        Route::get('caso-clinico', 'CasoClinicoController@index')
                            ->name('caso-clinico');
                        Route::post('caso-clinico', 'CasoClinicoController@post')
                            ->name('caso-clinico.post');
                        Route::post('caso-clinico/submissao', 'CasoClinicoController@submissao')
                            ->name('caso-clinico.submissao');
                    });

                Route::get('logout', 'Auth\LoginController@logout')
                    ->name('logout');
            });
    });
