<?php

use Illuminate\Support\Facades\Route;

Route::namespace('Avaliador')
    ->prefix('avaliador')
    ->name('avaliador.')
    ->group(function() {
        Route::namespace('Auth')
            ->middleware(['avaliador.guest'])
            ->group(function() {
                Route::get('login', 'LoginController@showLoginForm')
                    ->name('login');
                Route::post('login', 'LoginController@login')
                    ->name('login.post');
                Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')
                    ->name('password.request');
                Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')
                    ->name('password.email');
                Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')
                    ->name('password.reset');
                Route::post('password/reset', 'ResetPasswordController@reset')
                    ->name('password.update');
            });

        Route::middleware(['avaliador.auth'])
            ->group(function() {
                Route::get('/', 'AvaliadorController@index')
                    ->name('home');

                Route::get('casos-clinicos/{id?}', 'CasosClinicosController@index')
                    ->name('casos-clinicos');
                Route::post('casos-clinicos/{id}', 'CasosClinicosController@post')
                    ->name('casos-clinicos.post');

                Route::get('historico', 'HistoricoController@index')
                    ->name('historico');

                Route::get('logout', 'Auth\LoginController@logout')
                    ->name('logout');
            });
    });
