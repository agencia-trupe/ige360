const addNewRow = function(event) {
    event.preventDefault();

    const $wrapper = this.parentNode.parentNode;
    const template = $wrapper.querySelector('template');

    $wrapper.insertBefore(template.content.cloneNode(true), this.parentNode);
}

const deleteNewRow = function(event) {
    event.preventDefault();

    this.parentNode.parentNode.remove();
}

export default function() {
    if (!document.querySelector('.administracao-configuracoes')) {
        return false;
    }

    const $addBtn = document.querySelectorAll('.btn-adicionar');
    Array.from($addBtn).forEach(el => el.addEventListener('click', addNewRow));

    document.addEventListener('click', function(event) {
        if (event.target.classList.contains('btn-excluir-template')) {
            deleteNewRow.call(event.target, event);
        }
    });
}
