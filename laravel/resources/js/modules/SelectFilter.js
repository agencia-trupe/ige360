export default function() {
    const $selectFilter = document.querySelectorAll('.select-filter');

    Array.from($selectFilter).forEach(el => el.addEventListener('change', function() {
        window.location = this.querySelector(`option[value=${this.value}]`).dataset.route;
    }));
}
