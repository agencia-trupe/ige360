import flatpickr from 'flatpickr';
import { Portuguese } from 'flatpickr/dist/l10n/pt';
import 'flatpickr/dist/flatpickr.min.css';

export default function() {
    flatpickr('.datepicker', {
        dateFormat: "d/m/y",
        locale: Portuguese,
    });
}
