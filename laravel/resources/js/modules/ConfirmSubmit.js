import Swal from 'sweetalert2';

const confirmSubmit = function(event) {
    event.preventDefault();

    const $form = this.closest('form');

    const confirmationText = this.dataset.confirmacao;
    const formAction = this.getAttribute('formaction');

    Swal
        .fire({
            text: confirmationText,
            icon: 'success',
            showCancelButton: true,
            confirmButtonColor: '#18bc72',
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar',
        })
        .then((result) => {
            if (result.value) {
                if (formAction) {
                    $form.setAttribute('action', formAction);
                }

                $form.submit();
            }
        })
};

export default function() {
    const $submitBtn = document.querySelectorAll('.btn-enviar-confirmacao');
    Array.from($submitBtn).forEach(el => el.addEventListener('click', confirmSubmit));
}
