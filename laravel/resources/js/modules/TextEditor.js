import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

export default function() {
    const $editors = document.querySelectorAll('.text-editor');

    if ($editors.length) {
        $editors.forEach($editor => {
            ClassicEditor.create($editor, {
                toolbar: ['bold', 'italic', 'bulletedList'],
            });
        });
    }
}
