import Swal from 'sweetalert2';

const confirmDelete = function(event) {
    event.preventDefault();

    Swal
        .fire({
            text: "Deseja excluir esse registro?",
            icon: 'error',
            showCancelButton: true,
            confirmButtonColor: '#e74c3c',
            confirmButtonText: 'Excluir',
            cancelButtonText: 'Cancelar',
        })
        .then((result) => {
            if (result.value) {
                window.location = this.href;
            }
        })
};

export default function() {
    const $deleteBtn = document.querySelectorAll('.btn-excluir-confirmacao');
    Array.from($deleteBtn).forEach(el => el.addEventListener('click', confirmDelete));
}
