import IMask from 'imask';

export default function() {
    const $inputNota = document.getElementById('input-nota');

    if (!$inputNota) {
        return;
    }

    IMask($inputNota, {
        mask: Number,
        scale: 2,
        signed: false,
        thousandsSeparator: '',
        padFractionalZeros: true,
        normalizeZeros: true,
        radix: ',',
        mapToRadix: ['.'],
        min: 0,
        max: 10
    });
}
