import {
    Datepicker,
    TextEditor,
    Configuracoes,
    ConfirmDelete,
    ConfirmSubmit,
    SelectFilter,
    MaskNota,
} from './modules';

Datepicker();
TextEditor();
Configuracoes();
ConfirmDelete();
ConfirmSubmit();
SelectFilter();
MaskNota();
