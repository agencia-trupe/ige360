@extends('farmacovigilancia.common.template')

@section('content')

    <div class="main-padding farmacovigilancia-casos-clinicos">
        <div class="center">
            <h1>VALIDAÇÃO DO CASO CLÍNICO</h1>

            <form action="{{ route('farmacovigilancia.casos-clinicos.post', $casoClinico->id) }}" method="POST" class="form-caso">
                @csrf

                @include('common.flash')

                <div class="row-caso">
                    <label>título do caso clínico</label>
                    <input type="text" name="titulo" value="{{ old('titulo') ?: ($casoClinico ? $casoClinico->titulo : '') }}">
                </div>
                <div class="row-caso">
                    <label>relato do caso clínico</label>
                    <textarea name="relato" class="text-editor text-editor-caso text-editor-caso-grande">{!! old('relato') ?: ($casoClinico ? $casoClinico->relato : '') !!}</textarea>
                </div>
                <div class="row-caso">
                    <label>conclusão</label>
                    <textarea name="conclusao" class="text-editor text-editor-caso">{!! old('conclusao') ?: ($casoClinico ? $casoClinico->conclusao : '') !!}</textarea>
                </div>

                <div class="btn-group">
                    <input type="submit" value="SALVAR ALTERAÇÕES" class="btn btn-farmacovigilancia">
                    <input type="submit" value="VALIDAR E ENVIAR" class="btn btn-farmacovigilancia btn-enviar-confirmacao" data-confirmacao="Deseja validar e enviar o caso clínico? Essa ação não pode ser desfeita." formaction="{{ route('farmacovigilancia.casos-clinicos.validacao', $casoClinico->id) }}">
                </div>
            </form>
        </div>
    </div>

@endsection
