<header class="farmacovigilancia">
    <div class="logo">
        <img src="{{ asset('assets/img/layout/marca-concurso-ige360-marinho.svg') }}" alt="">
    </div>

    @auth('farmacovigilancia')
    <div class="user">
        <div class="center">
            <h2>FARMACOVIGILÂNCIA</h2>
            <h3>Olá {{ auth('farmacovigilancia')->user()->nome }}</h3>
            <a href="{{ route('farmacovigilancia.logout') }}" class="logout">SAIR</a>
        </div>
    </div>
    @endauth
</header>

@auth('farmacovigilancia')
<nav class="main-nav farmacovigilancia">
    <div class="center">
        <a href="{{ route('farmacovigilancia.casos-clinicos') }}" @if(Str::is('farmacovigilancia.casos-clinicos*', Route::currentRouteName())) class="active" @endif>
            CASOS CLÍNICOS
        </a>
        <a href="{{ route('farmacovigilancia.historico') }}" @if(Str::is('farmacovigilancia.historico*', Route::currentRouteName())) class="active" @endif>
            HISTÓRICO
        </a>
    </div>
</nav>
@endauth
