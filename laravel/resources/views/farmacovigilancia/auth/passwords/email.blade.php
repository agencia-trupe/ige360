@extends('farmacovigilancia.common.template')

@section('content')

    <div class="auth farmacovigilancia">
        <div class="title">
            <h1>FARMACOVIGILÂNCIA</h1>
        </div>

        <div class="auth-content center">
            <p>ESQUECI MINHA SENHA</p>

            <form method="POST" action="{{ route('farmacovigilancia.password.email') }}">
                @csrf

                @if(session('status'))
                    <div class="flash flash-success">
                        Um e-mail foi enviado com instruções para a redefinição de senha.
                    </div>
                @endif
                @error('email')
                    <div class="flash flash-error">
                        {{ $message }}
                    </div>
                @enderror

                <input type="email"name="email" value="{{ old('email') }}" required placeholder="e-mail" autofocus>
                <button type="submit">REDEFINIR SENHA</button>
            </form>

            <a href="{{ route('farmacovigilancia.login') }}" class="esqueci">
                &laquo; voltar
            </a>
        </div>
    </div>

@endsection
