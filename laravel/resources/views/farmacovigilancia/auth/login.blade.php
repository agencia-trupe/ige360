@extends('farmacovigilancia.common.template')

@section('content')

    <div class="auth farmacovigilancia">
        <div class="title">
            <h1>FARMACOVIGILÂNCIA</h1>
        </div>

        <div class="auth-content center">
            <p>No primeiro acesso clique em "ESQUECI MINHA SENHA" para receber o e-mail com link para definição de senha.</p>

            <form method="POST" action="{{ route('farmacovigilancia.login.post') }}">
                @csrf

                @error('email')
                <div class="flash flash-error">
                    Login e senha inválidos.
                </div>
                @enderror

                <input type="email" name="email" value="{{ old('email') }}" placeholder="login (e-mail)" required autofocus>
                <input type="password" name="password" placeholder="senha" required>
                <button type="submit">ACESSAR SISTEMA</button>
            </form>

            <a class="esqueci" href="{{ route('farmacovigilancia.password.request') }}">
                esqueci minha senha &raquo;
            </a>
        </div>
    </div>

@endsection
