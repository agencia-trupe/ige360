@extends('administracao.common.template')

@section('content')

    <div class="main-padding administracao-casos-clinicos">
        <div class="center">
            @include('common.flash')

            @include('administracao.casos-clinicos._filtros')

            @if(count($casosClinicos))
                <div class="casos-lista" style="--avaliadores: {{ $avaliadoresCount }}">
                    <div class="grid">
                        <span><br>CASO CLÍNICO / MÉDICO:</span>
                        <span>DATA DE<br>RECEBIM.:</span>
                        <span>ENVIO P/<br>AVALIAÇÃO:</span>
                        <span style="grid-column: span 2"><br>AVALIADO:</span>
                    </div>

                    @foreach($casosClinicos as $casoClinico)
                        <div class="grid @if(!$casoClinico->distribuido_em) grid-border @endif">
                            <div class="cell cell-titulo">
                                {{ $casoClinico->titulo }}<br>
                                Dr.(a) {{ $casoClinico->medico->nome }}
                            </div>
                            <div class="cell cell-data">
                                {!! $casoClinico->enviado_em->format('d/m/y \\<\\b\\r\\> H:i') !!}
                            </div>
                            <div class="cell cell-data">
                                {!! $casoClinico->distribuido_em
                                    ? $casoClinico->distribuido_em->format('d/m/y \\<\\b\\r\\> H:i')
                                    : '-'
                                !!}
                            </div>
                            @foreach($casoClinico->avaliacoes as $avaliacao)
                                <div class="cell cell-avaliacao">
                                    @if(!$avaliacao)
                                        <span style="color: #FC1C1C">X</span>
                                    @else
                                        <span>{{ $avaliacao->nota_formatada }}</span>
                                    @endif
                                </div>
                            @endforeach
                            <a href="{{ route('administracao.casos-clinicos.show', $casoClinico->id) }}" class="btn-caso">
                                {{ $casoClinico->distribuido_em ? 'VER' : 'ENVIAR PARA AVALIAR' }}
                            </a>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="nenhum">Nenhum caso clínico encontrado.</div>
            @endif
        </div>
    </div>

@endsection
