<div class="filtros">
    <div class="col">
        <span>Notas recebidas:</span>
        <select name="filtro" class="select-filter">
            <option value="null" data-route="{{ route('administracao.casos-clinicos', array_merge($_GET, ['filtro' => ''])) }}" @if(!request('filtro')) selected @endif>Selecione...</option>
            <option value="sem_avaliacoes" data-route="{{ route('administracao.casos-clinicos', array_merge($_GET, ['filtro' => 'sem_avaliacoes'])) }}" @if(request('filtro') === 'sem_avaliacoes') selected @endif>Sem nenhuma avaliação</option>
            <option value="avaliados" data-route="{{ route('administracao.casos-clinicos', array_merge($_GET, ['filtro' => 'avaliados'])) }}" @if(request('filtro') === 'avaliados') selected @endif>Avaliados</option>
        </select>
    </div>
    <div class="col">
        <span>Enviado para Avaliação:</span>
        <select name="distribuido" class="select-filter small">
            <option value="null" data-route="{{ route('administracao.casos-clinicos', array_merge($_GET, ['distribuido' => ''])) }}" @if(!request('distribuido')) selected @endif>Selecione...</option>
            <option value="sim" data-route="{{ route('administracao.casos-clinicos', array_merge($_GET, ['distribuido' => 'sim'])) }}" @if(request('distribuido') === 'sim') selected @endif>Sim</option>
            <option value="nao" data-route="{{ route('administracao.casos-clinicos', array_merge($_GET, ['distribuido' => 'nao'])) }}" @if(request('distribuido') === 'nao') selected @endif>Não</option>
        </select>
    </div>
    <div class="col">
        <span>Buscar por:</span>
        <form action="{{ route('administracao.casos-clinicos') }}" method="GET">
            @foreach($_GET as $key => $value)
                @if($key !== 'busca')
                    <input type="hidden" name="{{ $key }}" value="{{ $value }}">
                @endif
            @endforeach
            <div class="input-busca">
                <input type="text" name="busca" placeholder="Nome do Médico Coordenador ou e-mail" value="{{ request('busca') }}">
                <input type="submit">
            </div>
        </form>
    </div>
</div>
