@extends('administracao.common.template')

@section('content')

    <div class="main-padding administracao-casos-clinicos">
        <div class="center">
            <h1>CASO CLÍNICO &middot; VISUALIZAR DETALHES</h1>

            <div class="form-caso">
                <div class="grupo">
                    <h3>IDENTIFICAÇÃO DO GRUPO</h3>

                    <div class="grid">
                        <div class="col">
                            <p>Médico(a) Coordenador(a)</p>
                            <div class="input">
                                <label>nome</label>
                                <input type="text" name="nome" value="{{ $casoClinico->medico->nome }}" readonly>
                            </div>
                            <div class="input">
                                <label>CRM</label>
                                <input type="text" name="crm" value="{{ $casoClinico->medico->crm }}" readonly>
                            </div>
                            <div class="input">
                                <label>e-mail</label>
                                <input type="text" name="email" value="{{ $casoClinico->medico->email }}" readonly>
                            </div>
                            <div class="input">
                                <label>telefone</label>
                                <input type="text" name="telefone" value="{{ $casoClinico->medico->telefone }}" readonly>
                            </div>
                        </div>
                        <div class="col">
                            <p>Médicos(as) participantes</p>
                            @foreach(range(0, 4) as $i)
                                <div class="row">
                                    <div class="input">
                                        <label>nome</label>
                                        <input type="text" name="participantes[{{ $i }}][nome]" value="{{ $casoClinico && array_key_exists($i, $casoClinico->participantes) ? $casoClinico->participantes[$i]->nome : '' }}" readonly>
                                    </div>
                                    <div class="input">
                                        <div class="input">
                                            <label>CRM</label>
                                            <input type="text" name="participantes[{{ $i }}][crm]" value="{{ $casoClinico && array_key_exists($i, $casoClinico->participantes) ? $casoClinico->participantes[$i]->crm : '' }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="row-caso">
                    <label>título do caso clínico</label>
                    <div class="text-editor-readonly">
                        <p>{{ $casoClinico->titulo }}</p>
                    </div>
                </div>
                <div class="row-caso">
                    <label>relato do caso clínico</label>
                    <div class="text-editor-readonly">{!! $casoClinico->relato !!}</div>
                </div>
                <div class="row-caso">
                    <label>conclusão</label>
                    <div class="text-editor-readonly">{!! $casoClinico->conclusao !!}</div>
                </div>
                <div class="row-caso">
                    <label>referências bibliográficas</label>
                    <div class="text-editor-readonly">{!! $casoClinico->referencias_bibliograficas !!}</div>
                </div>

                <div class="status-avaliadores">
                    <div class="distribuido-em">
                        ENVIADO PARA AVALIADORES EM:
                        <span>{{ $casoClinico->distribuido_em->format('d/m/y - H:i') }}</span>
                    </div>
                    <div class="grid" style="--rows: {{ ceil(count($casoClinico->avaliacoes) / 2) }}">
                        @foreach($casoClinico->avaliacoes as $key => $avaliacao)
                            <div @if(!$avaliacao) class="highlight" @endif>
                                <span>AVALIADOR {{ $key + 1 }}</span>
                                <span>
                                    @if(!$avaliacao)
                                        <strong>NOTA NÃO APLICADA</strong>
                                    @else
                                        AVALIADO EM
                                        {{ $avaliacao->created_at->format('d/m/y - H:i') }}
                                    @endif
                                </span>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="btn-group">
                    <a href="{{ route('administracao.casos-clinicos') }}" class="btn btn-administracao">VOLTAR</a>
                </div>
            </div>
        </div>
    </div>

@endsection
