@extends('administracao.common.template')

@section('content')

    <div class="main-padding administracao-casos-clinicos">
        <div class="center">
            <h1>CASO CLÍNICO &middot; ENVIAR PARA AVALIAR</h1>

            <form action="{{ route('administracao.casos-clinicos.post', $casoClinico->id) }}" method="POST" class="form-caso">
                @csrf

                @include('common.flash')

                <div class="grupo">
                    <h3>IDENTIFICAÇÃO DO GRUPO</h3>

                    <div class="grid">
                        <div class="col">
                            <p>Médico(a) Coordenador(a)</p>
                            <div class="input">
                                <label>nome</label>
                                <input type="text" name="nome" value="{{ old('nome') ?: $casoClinico->medico->nome }}">
                            </div>
                            <div class="input">
                                <label>CRM</label>
                                <input type="text" name="crm" value="{{ old('crm') ?: $casoClinico->medico->crm }}">
                            </div>
                            <div class="input">
                                <label>e-mail</label>
                                <input type="text" name="email" value="{{ $casoClinico->medico->email }}" readonly>
                            </div>
                            <div class="input">
                                <label>telefone</label>
                                <input type="text" name="telefone" value="{{ old('telefone') ?: $casoClinico->medico->telefone }}">
                            </div>
                        </div>
                        <div class="col">
                            <p>Médicos(as) participantes</p>
                            @foreach(range(0, 4) as $i)
                                <div class="row">
                                    <div class="input">
                                        <label>nome</label>
                                        <input type="text" name="participantes[{{ $i }}][nome]" value="{{ old("participantes.$i.nome") ?: ($casoClinico && array_key_exists($i, $casoClinico->participantes) ? $casoClinico->participantes[$i]->nome : '') }}">
                                    </div>
                                    <div class="input">
                                        <div class="input">
                                            <label>CRM</label>
                                            <input type="text" name="participantes[{{ $i }}][crm]" value="{{ old("participantes.$i.crm") ?: ($casoClinico && array_key_exists($i, $casoClinico->participantes) ? $casoClinico->participantes[$i]->crm : '') }}">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <h2>CONTEÚDO DO CASO CLÍNICO</h2>

                <div class="row-caso">
                    <label>título do caso clínico</label>
                    <input type="text" name="titulo" value="{{ old('titulo') ?: ($casoClinico ? $casoClinico->titulo : '') }}">
                </div>
                <div class="row-caso">
                    <label>relato do caso clínico</label>
                    <textarea name="relato" class="text-editor text-editor-caso text-editor-caso-grande">{!! old('relato') ?: ($casoClinico ? $casoClinico->relato : '') !!}</textarea>
                </div>
                <div class="row-caso">
                    <label>conclusão</label>
                    <textarea name="conclusao" class="text-editor text-editor-caso">{!! old('conclusao') ?: ($casoClinico ? $casoClinico->conclusao : '') !!}</textarea>
                </div>
                <div class="row-caso">
                    <label>referências bibliográficas</label>
                    <textarea name="referencias_bibliograficas" class="text-editor text-editor-caso">{!! old('referencias_bibliograficas') ?: ($casoClinico ? $casoClinico->referencias_bibliograficas : '') !!}</textarea>
                </div>

                <div class="btn-group">
                    <input type="submit" value="SALVAR ALTERAÇÕES" class="btn btn-administracao">
                    <input type="submit" value="DISTRIBUIR PARA AVALIADORES" class="btn btn-administracao btn-enviar-confirmacao" data-confirmacao="Deseja distribuir o caso clínico para avaliadores? Essa ação não pode ser desfeita." formaction="{{ route('administracao.casos-clinicos.distribuir', $casoClinico->id) }}">
                </div>
            </form>
        </div>
    </div>

@endsection
