@extends('administracao.common.template')

@section('content')

    <div class="main-padding administracao-resultados">
        <div class="center">
            <h1>RESULTADOS</h1>

            @if(count($resultados))
                @foreach($resultados as $key => $casoClinico)
                    <div class="resultado-row">
                        <div class="posicao-nota">
                            <div class="posicao">{{ $key + 1 }}&ordm; LUGAR</div>
                            <div class="nota">
                                <span>NOTA:</span>
                                {{ $casoClinico->nota_resultado }}
                            </div>
                        </div>

                        <div class="caso">
                            <h2>{{ $casoClinico->titulo }}</h2>
                            <h3>Dr.(a) {{ $casoClinico->medico->nome }} - CRM {{ $casoClinico->medico->crm }}</h3>

                            @if($casoClinico->participantes)
                                @foreach($casoClinico->participantes as $participante)
                                    @if($participante->nome && $participante->crm)
                                        <p>Dr.(a) {{ $participante->nome }} - CRM {{ $participante->crm }}</p>
                                    @endif
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endforeach
            @else
                <div class="nenhum">Nenhum caso clínico para exibir.</div>
            @endif
        </div>
    </div>

@endsection
