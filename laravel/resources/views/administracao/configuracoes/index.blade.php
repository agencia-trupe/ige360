@extends('administracao.common.template')

@section('content')

    <div class="main-padding administracao-configuracoes">
        <div class="center">
            <form action="{{ route('administracao.configuracoes.post') }}" method="POST">
                @csrf

                @include('common.flash')

                <div class="block">
                    @foreach($grupos as $grupo)
                    <div class="row">
                        <label>Data limite - Grupo {{ $grupo->id }}:</label>
                        <div>
                            <input type="hidden" name="grupo[id][]" value="{{ $grupo->id }}">
                            <input type="text" name="grupo[data_limite][]" value="{{ $grupo->data_limite ? $grupo->data_limite->format('d/m/y') : null }}" class="datepicker" autocomplete="null">
                        </div>
                    </div>
                    @endforeach
                </div>

                <div class="block">
                    @foreach($avaliadores as $avaliador)
                    <div class="row">
                        <label>Avaliador {{ $avaliador->id }}:</label>
                        <div>
                            <input type="hidden" name="avaliador[id][]" value="{{ $avaliador->id }}">
                            <input type="text" name="avaliador[nome][]" value="{{ $avaliador->nome }}" placeholder="Nome" required autocomplete="null">
                            <input type="email" name="avaliador[email][]" value="{{ $avaliador->email }}" placeholder="E-mail" required autocomplete="null">
                        </div>
                    </div>
                    @endforeach
                </div>

                <div class="block">
                    <template>
                        <div class="row">
                            <label>Farmacovigilância:</label>
                            <div>
                                <input type="hidden" name="farmacovigilante[id][]" value="create">
                                <input type="text" name="farmacovigilante[nome][]" placeholder="Nome" required autocomplete="null">
                                <input type="email" name="farmacovigilante[email][]" placeholder="E-mail" required autocomplete="null">
                                <a href="#" class="btn-excluir btn-excluir-template">excluir</a>
                            </div>
                        </div>
                    </template>
                    @foreach($farmacovigilantes as $farmacovigilante)
                    <div class="row">
                        <label>Farmacovigilância:</label>
                        <div>
                            <input type="hidden" name="farmacovigilante[id][]" value="{{ $farmacovigilante->id }}">
                            <input type="text" name="farmacovigilante[nome][]" value="{{ $farmacovigilante->nome }}" placeholder="Nome" required autocomplete="null">
                            <input type="email" name="farmacovigilante[email][]" value="{{ $farmacovigilante->email }}" placeholder="E-mail" required autocomplete="null">
                            <a href="{{ route('administracao.configuracoes.delete', ['farmacovigilante', $farmacovigilante->id]) }}" class="btn-excluir btn-excluir-confirmacao">excluir</a>
                        </div>
                    </div>
                    @endforeach
                    <div class="row">
                        <label></label>
                        <a href="#" class="btn-adicionar">
                            ADICIONAR USUÁRIO FARMACOVIGILÂNCIA
                        </a>
                    </div>
                </div>

                <div class="block">
                    <template>
                        <div class="row">
                            <label>Usuário Administrativo:</label>
                            <div>
                                <input type="hidden" name="administrador[id][]" value="create">
                                <input type="text" name="administrador[nome][]" placeholder="Nome" required autocomplete="null">
                                <input type="email" name="administrador[email][]" placeholder="E-mail" required autocomplete="null">
                                <a href="#" class="btn-excluir btn-excluir-template">excluir</a>
                            </div>
                        </div>
                    </template>
                    @foreach($administradores as $administrador)
                    <div class="row">
                        <label>Usuário Administrativo:</label>
                        <div>
                            <input type="hidden" name="administrador[id][]" value="{{ $administrador->id }}">
                            <input type="text" name="administrador[nome][]" value="{{ $administrador->nome }}" placeholder="Nome" required autocomplete="null">
                            <input type="email" name="administrador[email][]" value="{{ $administrador->email }}" placeholder="E-mail" required autocomplete="null">
                            @if($administrador->id !== auth('administracao')->user()->id)
                                <a href="{{ route('administracao.configuracoes.delete', ['administrador', $administrador->id]) }}" class="btn-excluir btn-excluir-confirmacao">excluir</a>
                            @endif
                        </div>
                    </div>
                    @endforeach
                    <div class="row">
                        <label></label>
                        <a href="#" class="btn-adicionar">
                            ADICIONAR USUÁRIO ADMINISTRATIVO
                        </a>
                    </div>
                </div>

                <div class="btn-group">
                    <input type="submit" class="btn btn-administracao" value="SALVAR">
                </div>
            </form>
        </div>
    </div>

@endsection
