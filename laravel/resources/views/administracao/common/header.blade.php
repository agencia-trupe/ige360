<header class="administracao">
    <div class="logo">
        <img src="{{ asset('assets/img/layout/marca-concurso-ige360-marinho.svg') }}" alt="">
    </div>

    @auth('administracao')
    <div class="user">
        <div class="center">
            <h2>ADMINISTRADOR</h2>
            <h3>Olá {{ auth('administracao')->user()->nome }}</h3>
            <a href="{{ route('administracao.logout') }}" class="logout">SAIR</a>
        </div>
    </div>
    @endauth
</header>

@auth('administracao')
<nav class="main-nav administracao">
    <div class="center">
        <a href="{{ route('administracao.coordenadores') }}" @if(Str::is('administracao.coordenadores*', Route::currentRouteName())) class="active" @endif>
            COORDENADORES
        </a>
        <a href="{{ route('administracao.casos-clinicos') }}" @if(Str::is('administracao.casos-clinicos*', Route::currentRouteName())) class="active" @endif>
            CASOS CLÍNICOS
        </a>
        <a href="{{ route('administracao.resultados') }}" @if(Str::is('administracao.resultados*', Route::currentRouteName())) class="active" @endif>
            RESULTADOS
        </a>
        <a href="{{ route('administracao.configuracoes') }}" @if(Str::is('administracao.configuracoes*', Route::currentRouteName())) class="active" @endif>
            CONFIGURAÇÕES
        </a>
    </div>
</nav>
@endauth
