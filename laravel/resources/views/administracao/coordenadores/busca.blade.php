@extends('administracao.common.template')

@section('content')

    <div class="main-padding administracao-coordenadores">
        <div class="center grupo">
            <div class="grupo-header">
                <h2>
                    <span>
                        RESULTADO DA BUSCA
                    </span>
                </h2>

                <form action="{{ route('administracao.coordenadores') }}" method="GET">
                    <div class="input-busca">
                        <input type="text" name="busca" placeholder="BUSCAR COORDENADOR [nome ou e-mail]" value="{{ request('busca') }}">
                        <input type="submit">
                    </div>
                </form>
            </div>

            @if(count($medicos))
                <div class="busca-lista">
                    <div class="busca-grid">
                        <span>GRUPO:</span>
                        <span>COORDENADOR:</span>
                        <span>EMAIL:</span>
                    </div>

                    @foreach($medicos as $medico)
                        <div class="busca-grid">
                            <div class="cell">
                                GRUPO {{ $medico->grupo->id }} &middot;
                                {{ $medico->grupo->data_limite ? $medico->grupo->data_limite->format('d/m/y') : '-' }}
                            </div>
                            <div class="cell">{{ $medico->nome }}</div>
                            <div class="cell">{{ $medico->email }}</div>
                            <a href="{{ route('administracao.coordenadores.grupo', $medico->grupo->id) }}" class="btn-grupo">VER NO GRUPO</a>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="nenhum">Nenhum coordenador encontrado.</div>
            @endif
        </div>
    </div>

@endsection
