@extends('administracao.common.template')

@section('content')

    <div class="main-padding administracao-coordenadores">
        <div class="center texto-convite">
            <h2>TEXTO DO CONVITE POR E-MAIL</h2>
            <p>Texto do convite que é enviado ao Médico(a) Coordenador(a)</p>

            <form action="{{ route('administracao.coordenadores.texto-convite.post') }}" method="POST">
                @csrf

                @include('common.flash')

                <textarea name="texto" class="text-editor">{!! $textoConvite->texto !!}</textarea>

                <p>
                    Após o texto personalizado segue-se:<br>
                    <em>
                        Para acessar o sistema pela primeira vez e criar sua senha acesse este link:
                        <strong>www.ige360.com.br</strong><br>
                        Seu prazo de submissão do Caso Clínico é:
                        <strong>&lt;data de acordo com o grupo que o coordenador pertence&gt;</strong>
                    </em>
                </p>

                <div class="btn-group btn-group-center">
                    <input type="submit" class="btn btn-administracao btn-inline" value="SALVAR">
                    <a href="{{ route('administracao.coordenadores') }}" class="btn btn-administracao btn-inline">
                        VOLTAR
                    </a>
                </div>
            </form>
        </div>
    </div>

@endsection
