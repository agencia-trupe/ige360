@extends('administracao.common.template')

@section('content')

    <div class="main-padding administracao-coordenadores">
        <div class="center grupo">
            <div class="grupo-header">
                <h2>
                    GRUPO {{ $grupo->id }} &middot;
                    <span>
                        DATA LIMITE DE ENVIO:
                        {{ $grupo->data_limite ? $grupo->data_limite->format('d/m/y') : '-' }}
                    </span>
                </h2>

                <form action="{{ route('administracao.coordenadores.grupo', $grupo->id) }}" method="GET">
                    <div class="input-busca">
                        <input type="text" name="busca" placeholder="BUSCAR COORDENADOR [nome ou e-mail]" value="{{ request('busca') }}">
                        <input type="submit">
                    </div>
                </form>
            </div>

            @include('common.flash')

            @if(count($medicos) || !request('busca'))
                <div class="grupo-lista">
                    <div class="grupo-grid">
                        <span>COORDENADOR:</span>
                        <span>EMAIL:</span>
                    </div>

                    @if(!request('busca'))
                        <form action="{{ route('administracao.coordenadores.grupo.post', $grupo->id) }}" method="POST" class="grupo-grid">
                            @csrf
                            <input type="text" name="nome" placeholder="nome" required>
                            <input type="email" name="email" placeholder="e-mail" required>
                            <input type="submit" class="btn-grupo" value="INSERIR">
                        </form>
                        @endif

                    @foreach($medicos as $medico)
                        <div class="grupo-grid">
                            <div class="cell">{{ $medico->nome }}</div>
                            <div class="cell">{{ $medico->email }}</div>
                            @if($medico->casoClinico && $medico->casoClinico->enviado_em)
                                <div class="cell-caso">
                                    PRIMEIRO CASO CLÍNICO ENVIADO EM
                                    {{ $medico->casoClinico->enviado_em->format('d/m/y H:i') }}
                                </div>
                            @else
                                <a href="{{ route('administracao.coordenadores.grupo.delete', [$grupo->id, $medico->id]) }}" class="btn-grupo btn-excluir-confirmacao">EXCLUIR</a>
                                @if($medico->cadastrado_em)
                                    <div class="cell-convite span-2">
                                        CADASTRADO EM
                                        {{ $medico->cadastrado_em->format('d/m/y H:i') }}
                                    </div>
                                @else
                                    <a href="{{ route('administracao.coordenadores.grupo.convite', [$grupo->id, $medico->id]) }}" class="btn-grupo">
                                        {{ $medico->convite_enviado_em ? 'REENVIAR CONVITE' : 'ENVIAR CONVITE' }}
                                    </a>
                                    @if($medico->convite_reenviado_em)
                                        <div class="cell-convite">
                                            CONVITE REENVIADO EM<br>
                                            {{ $medico->convite_reenviado_em->format('d/m/y H:i') }}
                                        </div>
                                    @elseif($medico->convite_enviado_em)
                                        <div class="cell-convite">
                                            CONVITE ENVIADO EM<br>
                                            {{ $medico->convite_enviado_em->format('d/m/y H:i') }}
                                        </div>
                                    @endif
                                @endif
                            @endif
                        </div>
                    @endforeach
                </div>
            @else
                <div class="nenhum">Nenhum coordenador encontrado.</div>
            @endif
        </div>
    </div>

@endsection
