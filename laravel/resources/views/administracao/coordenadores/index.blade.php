@extends('administracao.common.template')

@section('content')

    <div class="main-padding administracao-coordenadores">
        <div class="center">
            <div class="grid-index">
                <a href="{{ route('administracao.coordenadores.texto-convite') }}" class="btn btn-administracao btn-block">
                    TEXTO DO CONVITE
                </a>

                <form action="{{ route('administracao.coordenadores') }}" method="GET">
                    <div class="input-busca">
                        <input type="text" name="busca" placeholder="BUSCAR COORDENADOR [nome ou e-mail]">
                        <input type="submit">
                    </div>
                </form>
            </div>

            <div class="grupos">
                @foreach($grupos as $grupo)
                <div class="grid-index">
                    <div class="grupo-data">
                        GRUPO {{ $grupo->id }}<br>
                        DATA LIMITE: {{ $grupo->data_limite ? $grupo->data_limite->format('d/m/y') : '-' }}
                    </div>
                    <a href="{{ route('administracao.coordenadores.grupo', $grupo->id) }}" class="btn btn-block btn-administracao">
                        VER GRUPO | ADICIONAR COORDENADORES
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
