@extends('administracao.common.template')

@section('content')

    <div class="auth administracao">
        <div class="title">
            <h1>ADMINISTRAÇÃO</h1>
        </div>

        <div class="auth-content center">
            <p>REDEFINIR SENHA</p>

            <form method="POST" action="{{ route('administracao.password.update') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">
                <input type="hidden" name="email" value="{{ $email }}">

                @error('password')
                    <div class="flash flash-error">
                        {{ $message }}
                    </div>
                @enderror
                @error('email')
                    <div class="flash flash-error">
                        {{ $message }}
                    </div>
                @enderror

                <input type="password" name="password" placeholder="senha" required autofocus>
                <input type="password" name="password_confirmation" placeholder="confirmar senha" required>

                <button type="submit">REDEFINIR SENHA</button>
            </form>

            <a href="{{ route('administracao.login') }}" class="esqueci">
                &laquo; voltar
            </a>
        </div>
    </div>

@endsection
