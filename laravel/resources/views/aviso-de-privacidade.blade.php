@extends('medico.common.template')

@section('content')

    <style>
        .aviso-de-privacidade {
            line-height: 1.5;
            padding: 40px 0;
        }

        .aviso-de-privacidade a {
            color: #2c5680;
            text-decoration: underline;
        }

        .sublinhado {
            text-decoration: underline;
        }

        .box {
            border: 1px solid #444;
            padding: 10px 20px;
        }

        .box + .box {
            margin-top: 20px;
        }
    </style>

    <div class="center aviso-de-privacidade">
        <p>
        Você está recebendo este Aviso de Privacidade porque você está visitando um site de uma das empresas do grupo Novartis. Como resultado, esta empresa está processando informações sobre você que constituem <strong>"Dados pessoais"</strong> e a Novartis considera a proteção de seus dados pessoais e de sua privacidade uma questão muito importante.
      </p>

      <p>
        Neste Aviso de Privacidade, <strong>"nós"</strong> ou <strong>"nosso"</strong> refere-se a Novartis Biociências, S.A., com sede em Avenida Professor Vicente Rao, 90, Brooklin Paulista, CEP 04636-000, São Paulo, Brasil, que é a entidade responsável pelo tratamento dos seus dados pessoais.
      </p>

      <p>
        Este Aviso de Privacidade divide-se em duas partes. A Parte I contém informação prática sobre dados pessoais específicos que tratamos quando visita o nosso website, porque é que tratamentos estes dados e como. A Parte II contém informação geral acerca dos padrões técnicos ou dados pessoais de navegação que estamos a tratar sobre os visitantes dos nossos websites ou utilizadores das nossas apps, a base legal para utilizar os seus dados pessoais, bem como os seus direitos relativamente a todos os dados pessoais recolhidos sobre si.
      </p>

      <p>
        Convidamo-lo(a) a ler cuidadosamente este Aviso de Privacidade e para qualquer questão adicional relativa ao tratamento dos seus dados pessoais convidamo-lo(a) a contactar a Novartis Biociências através do e-mail <a href="mailto:brasil.privacy_office@novartis.com">brasil.privacy_office@novartis.com</a>.
      </p>

      <p>
        Este Aviso de Privacidade é dividido em duas partes. A Parte I contém informações práticas sobre o processamento de dados pessoais <span class="sublinhado">específicos</span> quando você visita o nosso site (o "Site"), por que e como esses dados são processados. A parte II contém informações mais gerais sobre o processamento de dados pessoais transacionais ou técnicos <span class="sublinhado">padrão</span> sobre os visitantes de nossos sites, a base legal para o uso de seus dados pessoais, bem como os seus direitos em relação a todos os dados pessoais coletados sobre você.
      </p>

      <p>
        Convidamos você a ler atentamente este Aviso de Privacidade, pois contém informações importantes. Para qualquer outra questão em relação ao processamento de seus dados pessoais, nós convidamos você a entrar em contato com a Novartis Biociências através do e-mail <a href="mailto:brasil.privacy_office@novartis.com">brasil.privacy_office@novartis.com</a> ..
      </p>

      <h2>
        Parte I – Informação Principal
      </h2>

      <p>
        A Novartis Biociências processa dados pessoais sobre você quando você visita nosso site. O site foi desenvolvido para permitir a sua utilização enquanto participante no programa de casos clínicos e dar a você enquanto profissional da saúde a possibilidade de partilhar o caso clínico e ter o mesmo avaliado por um conjunto de profissionais de saúde pré-selecionados pela Novartis Biociências.
      </p>

      <div class="box">
        <p class="sublinhado">
          Dados pessoais especificamente coletados
        </p>
        <p>
          Para este efeito, iremos coletamos os seguintes específicos dados pessoais sobre você:
        </p>
        <p>
          Dados de Identificação Geral: Nome e sobrenome, e-mail, endereço e telefone/celular;
        </p>
        <p>
          Outros Dados de Identificação: número de carteira profissional (CRM);
        </p>
        <p>
          Outros Dados: Descrição do caso clínico apresentado a programa
        </p>
        <p>
          Esta informação é fornecida à Novartis Biociências diretamente por si (por exemplo, quando nos forneceu os seus dados para lhe entregarmos a senha de acesso inicial ao site, quando assinou o  termo de participação no programa ou quando interage com o website), fornecida por terceiros ou obtida através de fontes públicas confiáveis.
        </p>
        <p>
          Observe que contamos também com o uso habitual de cookies e outras tecnologias para o propósito padrão enunciado na Parte II abaixo.
        </p>
      </div>

      <div class="box">
        <p class="sublinhado">
          Finalidades específicas para coletarmos os seus dados pessoais
        </p>
        <p>
          A informação coletada será utilizada para as seguintes finalidades específicas:
        </p>
        <p>
          Aceitar a sua participação no programa de casos clínicos para que o júri avalie o mesmo. Adicionalmente os seus dados de identificação juntamente com a descrição do caso clínico podem ser usados pela Novartis Biociências em publicações / materiais desenvolvidos ou apoiados pela Novartis Biociências conforme consta do Termo de Regulamento do programa.
        </p>
        <p>
          Observe que os dados também podem ser utilizados por nós para uma série de outros fins (por exemplo, para medir a utilização do nosso site), tal como definido na Parte II abaixo.
        </p>
      </div>

      <div class="box">
        <p class="sublinhado">
          Entidades terceiras específicas com quem compartilhamos os seus dados
        </p>
        <p class="sublinhado">
          Terceiros específicos com os quais compartilhamos os seus dados pessoais
        </p>
        <p>
          Poderemos divulgar suas informações pessoais a terceiros:
        </p>
        <ul>
          <li>
            No caso de vender ou comprar qualquer negócio ou bens, nesse caso podemos divulgar os seus dados pessoais ao vendedor ou comprador de tais negócios ou ativos.
          </li>
          <li>
            Se a Novartis Biociências ou substancialmente todos os nossos ativos forem adquiridos por terceiros, nesse caso os dados pessoais detidos por nós sobre os nossos visitantes/clientes serão um dos ativos transferidos.
          </li>
          <li>
            Se formos obrigados a divulgar ou compartilhar seus dados pessoais, a fim de cumprir qualquer obrigação legal, ou para fazer cumprir ou aplicar nossos termos de uso e outros acordos; ou para proteger os direitos, propriedade ou segurança da Novartis Biociências, de nossos clientes ou outros..
          </li>
        </ul>
        <p>
          Observe que nós também podemos ter de partilhar os seus dados com um número de outros destinatários (por exemplo, outra entidade do grupo Novartis se a entidade que coleta os dados não é a mesma que os usará e qualquer prestador de serviços de tecnologias de informação ou de armazenagem de informação), mas sempre sob condições estritas, como explicado na Parte II.
        </p>
      </div>

      <div class="box">
        <p class="sublinhado">
          Período de conservação
        </p>
        <p>
          Nós armazenaremos apenas os dados pessoais e os dados pessoais referidos na Parte II durante a vigência do programa e enquanto mantivermos uma relação profissional consigo.
        </p>
      </div>

      <div class="box">
        <p class="sublinhado">
          Cookies e outras tecnologias semelhantes
        </p>
        <p>
          Por favor tenha em atenção que também dependemos dos cookies e de outras tecnologias usuais para as finalidades padrão descritas na Parte II abaixo (por exemplo, assegurar o correto funcionamento do nosso website ou app).
        </p>
      </div>

      <div class="box">
        <p class="sublinhado">
          Ponto de Contacto
        </p>
        <p>
          Caso tenha alguma questão sobre o tratamento dos seus dados pessoais no contexto aqui descrito, por favor contacte a Novartis Biociências através do e-mail <a href="mailto:brasil.privacy_office@novartis.com">brasil.privacy_office@novartis.com</a>.
        </p>
      </div>

      <h2>
        Parte II - Informações gerais
      </h2>

      <p>
        A segunda parte deste Aviso de Privacidade define mais detalhadamente em que contexto estamos processando os dados pessoais e explica os seus direitos e obrigações ao fazê-lo.
      </p>

      <p>
        <strong>1 Para que propósito utilizamos os seus dados pessoais e por quê isto é justificado?</strong>
      </p>
      <p>
        <strong>1.1</strong> Base legal para o processamento
      </p>
      <p>
        Não processaremos os seus dados pessoais, se não tivermos uma justificação adequada prevista em lei para tal propósito. Assim, processaremos apenas os seus dados pessoais se:
      </p>
      <p>
        ☒	Obtivemos seus dados com o seu consentimento prévio;
      </p>
      <p>
        ☐	O processamento é necessário para cumprirmos com as nossas obrigações contratuais perante a você ou para tomarmos passos pré-contratuais a seu pedido;
      </p>
      <p>
        ☐	O processamento é necessário para cumprir com as nossas obrigações legais ou regulamentares;
      </p>
      <p>
        ☐	O processamento é necessário para proteger os seus interesses vitais, ou de outra pessoa; ou
      </p>
      <p>
        ☒ 	O processamento é necessário para nossos interesses legítimos e não afetam indevidamente seus interesses ou os direitos e liberdades fundamentais.
      </p>
      <p>
        Observe que, durante o processamento de seus dados pessoais com base nesta última premissa, procuramos sempre manter um equilíbrio entre os nossos interesses legítimos e sua privacidade. Exemplos de tais "interesses legítimos" são as atividades de processamento de dados realizadas:
      </p>
      <ul>
        <li>
          para beneficiar a relação custo-benefício dos serviços (por exemplo, podemos optar por certas plataformas oferecidas pelos fornecedores para processar dados);
        </li>
        <li>
          para oferecer produtos e serviços aos consumidores;
        </li>
        <li>
          para evitar fraude ou atividade criminosa, desvios de serviços e produtos, bem como a segurança dos sistemas de TI, arquitetura e redes;
        </li>
        <li>
          para vender qualquer parte de nossos negócios ou seus ativos ou para permitir a aquisição da totalidade ou de parte de nossos negócios ou bens de terceiros.
        </li>
      </ul>
      <p>
        <strong>1.2</strong> Propósito do processamento
      </p>
      <p>
        Nós sempre processaremos os seus dados pessoais para uma finalidade específica e processaremos apenas os dados pessoais que são relevantes para a realização desse propósito. Além disso, para os propósitos específicos identificados na Parte I acima, nós também processaremos os seus dados pessoais para os propósitos gerais a seguir:
      </p>
      <ul>
        <li>
          melhorar nossos produtos e serviços, bem como os dos nossos parceiros;
        </li>
        <li>
          comercialização de produtos;
        </li>
        <li>
          fornecer informações adequadas e atualizadas sobre doenças, medicamentos, bem como produtos e serviços;
        </li>
        <li>
          responder a quaisquer perguntas ou pedidos que você possa ter;
        </li>
        <li>
          arquivamento e manutenção de registros; e
        </li>
        <li>
          quaisquer outros fins impostos pela lei e autoridades (tais como propósitos de segurança de medicamentos).
        </li>
      </ul>

      <p>
        <strong>2 Quem tem acesso a seus dados pessoais e a quem são transferidos?</strong>
      </p>

      <p>
        Nós não vendemos, compartilhamos ou, de qualquer outro modo, transferimos seus dados pessoais a terceiros que não sejam os indicados na presente Política de Privacidade.
      </p>

      <p>
        Durante as nossas atividades e para os mesmos propósitos listados na presente Política de Privacidade, seus dados pessoais podem ser acessados por ou transferidos para terceiros específicos identificados na Parte I da presente Política de Privacidade e as seguintes categorias de destinatários, apenas se têm a necessidade de conhecer tais dados, para alcançarmos tais propósitos:
      </p>

      <ul>
        <li>
          nosso pessoal (incluindo funcionários, departamentos ou outras empresas do grupo Novartis);
        </li>
        <li>
          nossos outros fornecedores e prestadores de serviços que fornecem produtos e prestam serviços a nós;
        </li>
        <li>
          nossos provedores de sistemas de TI prestadores de serviços em nuvem, provedores e consultores de banco de dados;
        </li>
        <li>
          os nossos parceiros de negócios que oferecem produtos ou serviços em conjunto conosco;
        </li>
        <li>
          qualquer terceiro a quem se atribuir ou substituir qualquer dos nossos direitos ou obrigações;
        </li>
        <li>
          nossos consultores e advogados externos no contexto da venda ou transferência de qualquer parte de nossos negócios ou seus ativos
        </li>
      </ul>

      <p>
        Os terceiros são obrigados a proteger a confidencialidade e a segurança de seus dados pessoais, em conformidade com a legislação aplicável.
      </p>
      <p>
        Seus dados pessoais também podem ser acessados por ou transferidos a qualquer órgão público ou tribunal, nacional e/ou internacional, de aplicação regulamentar, aos quais somos obrigados a fazê-lo por lei ou regulamento aplicável ou a seu pedido.
      </p>
      <p>
        Os dados pessoais que coletamos de você podem também ser processados, acessados ou armazenados em um país fora do país onde a Novartis Biociências está localizada, o qual possa não oferecer o mesmo nível de proteção de dados pessoais.
      </p>
      <p>
        Se transferirmos seus dados pessoais a empresas externas em outras jurisdições, certificaremo-nos de proteger seus dados pessoais (i) aplicando o nível de proteção exigido sob as leis de proteção de dados/privacidade locais aplicáveis à Novartis Biociências, (ii) atuando em conformidade com as nossas políticas e normas e, (iii) para as entidades Novartis localizadas no Espaço Econômico Europeu (ou seja, os Estados-Membros da UE e a Islândia, Liechtenstein e Noruega, os "EEE"), a menos que seja especificado o contrário, apenas a transferência de seus dados pessoais com base nas cláusulas contratuais aprovadas pela Comissão Europeia. Você pode solicitar informações adicionais em relação às transferências internacionais de dados pessoais e obter uma cópia da proteção adequada pelo exercício dos seus direitos, tal como definido na Seção 6 abaixo.
      </p>
      <p>
        Para transferências intragrupo de transferências de dados pessoais o grupo Novartis adota as regras societárias vinculativas, um sistema de princípios, normas e ferramentas, fornecidas pelo direito europeu, em um esforço para garantir níveis efetivos de proteção dos dados relativos às transferências de dados pessoais para fora dos EEE e da Suíça. Leia mais sobre as Regras Societárias Vinculativas da Novartis clicando aqui <a href="https://www.novartis.com/our-company/corporate-responsibility/doing-business-responsibly/ethics-compliance/data-privacy" target="_blank">Novartis Binding Corporate Rules</a>.
      </p>

      <p>
        <strong>3 Como protegemos os seus dados pessoais?</strong>
      </p>

      <p>
        Implementamos medidas técnicas e organizacionais adequadas para fornecer um nível de segurança e confidencialidade de seus dados pessoais. Essas medidas levam em conta:
      </p>

      <p>
        (i) O estado da arte e da tecnologia, em particular recorremos à transmissão de informação em rede de modo criptografado, asseguramos trilhas de auditoria das pessoas autorizadas a aceder aos seus dados pessoais e que tal acesso é realizado mediante identificadores de utilizadores únicos, com senhas de acesso pessoais e intransmissíveis que são periodicamente atualizadas;
      </p>

      <p>
        (ii) A hospedagem das informações, onde ficam armazenadas em ambientes seguros, controlados e rigorosamente avaliados, no que diz respeito aos requisitos técnicos de segurança da informação;
      </p>

      <p>
        (i) os custos de sua implementação;
      </p>

      <p>
        (ii) a natureza dos dados; e
      </p>

      <p>
        (iii) o risco do processamento.
      </p>

      <p>
        O propósito disto é protegê-lo contra a destruição acidental ou ilícita, a perda acidental, a alteração, a difusão ou o acesso não autorizado e contra qualquer outra forma de processamento ilícito.
      </p>

      <p>
        Além disso, durante a manipulação de seus dados pessoais, nós cumprimos com as seguintes obrigações:
      </p>

      <ul>
        <li>
          Coletamos e processamos apenas dados pessoais, que sejam adequados, pertinentes e não excessivos, conforme necessário para satisfazer os propósitos acima referidos;
        </li>
        <li>
          Garantimos que seus dados pessoais permanecem atualizados e precisos (para este último, podemos solicitar que você confirme os dados pessoais que temos sobre você e você também está convidado a informar-nos espontaneamente sempre que houver uma mudança em suas circunstâncias pessoais, para que possamos garantir que seus dados pessoais sejam mantidos atualizados); e
        </li>
        <li>
          Nós podemos processar dados confidenciais sobre você que você fornecer voluntariamente em conformidade com as regras de proteção de dados e de modo estritamente necessários para os propósitos indicados acima, e os dados sendo acessados e processados exclusivamente pelo pessoal relevante, sob a responsabilidade de um de nossos representantes, que está sujeito à obrigação de sigilo profissional ou de confidencialidade.
        </li>
      </ul>

      <p>
        <strong>4 Por quanto tempo protegemos os seus dados pessoais?</strong>
      </p>

      <p>
        Nós armazenaremos apenas os dados pessoais e os dados pessoais referidos na Parte II durante a vigência do programa e enquanto mantivermos uma relação profissional consigo.
      </p>

      <p>
        <strong>2 Como usamos cookies e outras tecnologias similares em nossos sites?</strong>
      </p>

      <p>
        <strong>2.1</strong> Cookies
      </p>

      <p>
        Cookies são pequenos arquivos de texto que são enviados para o seu computador quando você visita nossos sites. Usamos cookies para os propósitos acima definidos e em conformidade com este Aviso de Privacidade.
      </p>

      <p>
        Não usamos cookies para rastrear os visitantes individuais ou para identificá-los, mas para obter conhecimentos úteis sobre como nossos sites são usados para que possamos continuar a melhorá-los para nossos usuários. Dados pessoais gerados através de cookies são coletados em um formulário anônimo e sujeito ao seu direito de se opor a tal processamento de dados, conforme estabelecido a seguir.
      </p>

      <p>
        Usamos cookies neste Site para:
      </p>

      <p>
        1. Dirigir você às seções relevantes no Site;
      </p>
      <p>
        2. Assegurar que o Site ofereça uma aparência consistente em diferentes navegadores e dispositivos;
      </p>
      <p>
        3. Permitir que áreas complexas do Site funcionem; e
      </p>
      <p>
        4. Rastrear de modo anônimo, estatísticas agregadas sobre visitas ao site para nos ajudar a melhorar o desempenho do Site.
      </p>
      <p>
        Ao fazer isso, podemos instalar cookies que coletam o nome do domínio do usuário, provedor de internet, o sistema operacional e a data e hora de acesso.
      </p>
      <p>
        Abaixo são explicados os tipos genéricos de cookies que usamos e os seus efeitos.
      </p>
      <p class="sublinhado">
        Os tipos e propósitos dos Cookies
      </p>
      <ul>
        <li>
          Cookies de primeira parte: os cookies definidos pelo site visitado pelo usuário (o site exibido na janela da URL).
        </li>
        <li>
          Cookies de sessão: os cookies que expiram no final de uma sessão do navegador (começando no momento quando um usuário abre a janela do navegador, e terminando quando o usuário fecha o navegador).
        </li>
        <li>
          Cookies persistentes: cookies que "persistem" no dispositivo após o término de uma sessão do navegador e, portanto, podem permitir que as preferências ou ações do usuário sejam lembradas quando o site é revisitado.
        </li>
      </ul>
      <p class="sublinhado">
        Como usar as configurações de seu navegador para controlar e eliminar os cookies
      </p>
      <p>
        A maioria dos navegadores da web permitem o controle da maioria dos cookies através das configurações do navegador. Você pode configurar seu navegador para notificá-lo quando receber um cookie - isso permitirá que você decida se ou não você deseja aceitá-lo. Contudo, se você não aceitar um cookie, você pode não ser capaz de usar todas as funcionalidades do seu software do navegador.
      </p>
      <p>
        Para saber mais sobre cookies, incluindo como ver quais cookies foram definidos e como gerenciá-los e excluí-los, visite <a href="http://www.allaboutcookies.org" target="_blank">http://www.allaboutcookies.org</a> (link externo).
      </p>
      <p class="sublinhado">
        Marcadores da Internet
      </p>
      <p>
        Podemos usar marcadores da internet (também conhecidos como web beacons, action tags, single-pixel GIFs, clear GIFs, invisible GIFs e 1-by-1 GIFs) e os cookies neste Site e podemos implantar esses marcadores/cookies através de um parceiro de serviço de análise da web que pode ser localizado e armazenar a respectiva informação (incluindo o seu endereço IP), em um país estrangeiro. Esses marcadores/cookies são colocados em diferentes páginas do site. Nós utilizamos esta tecnologia para medir as respostas do usuário aos nossos sites (incluindo quantas vezes uma página é aberta e quais informações são consultadas), bem como para avaliar o seu uso deste Site.
      </p>
      <p>
        Nossos parceiros terceiros, provedores de serviços de hospedagem e/ou parceiros de serviço de análise da web podem coletar dados sobre o seu uso deste site por meio destes marcadores/cookies anônimos da Internet, e podem compor relatórios sobre a atividade do Site para nós e podem fornecer mais serviços que estão relacionados ao uso do site e da internet. Eles podem fornecer tais informações a outras partes, se houver um requisito legal para que o façam, ou se contratam outras partes para processar informações em seu nome.
      </p>
      <p>
        Este site utiliza o Google Analytics, um serviço de análise da web do Google Inc. ("Google"). O Google Analytics usa os chamados "cookies", arquivos de texto armazenados em seu computador, o que torna possível analisar a utilização do site por você. As informações produzidas pelo cookie sobre a sua utilização deste site são geralmente transmitidas a um servidor do Google nos EUA e lá são armazenadas. Em caso de ativação de anonimização de IP neste Site, o seu endereço IP é, no entanto, previamente abreviado pelo Google dentro dos Estados-Membros da União Europeia ou de outros estados parte do Acordo sobre o Espaço Econômico Europeu. Apenas em casos excepcionais o endereço IP completo é transmitido a um servidor do Google nos EUA e, então, abreviado. Se for atribuído pelo operador do site, o Google utilizará estas informações para avaliar a sua utilização do site, a fim de compilar relatórios sobre as atividades do site para os operadores do Site e para prestar serviços adicionais associados com o uso do Web site e o uso da Internet. O endereço IP transmitidos no âmbito do Google Analytics pelo seu navegador não é combinado com outros dados do Google. Você pode impedir a instalação de cookies através de uma configuração apropriada de seu navegador; contudo, gostaríamos de salientar que, neste caso, você pode não ser capaz de utilizar todas as funcionalidades deste Site.
      </p>
      <p>
        Você pode encontrar mais informações detalhadas sobre as condições de utilização e proteção de dados em <a href="http://www.google.com/analytics/terms/en.html" target="_blank">http://www.google.com/analytics/terms/en.html</a> ou em <a href="Https://www.google.co.uk/intl/en/policies/" target="_blank">Https://www.google.co.uk/intl/en/policies/</a> (links externos). Gostaríamos de salientar que neste Site, o Google Analytics tem sido expandido pelo código "anonymizeIp", a fim de garantir a coleta anônima de endereços IP (chamado de mascaramento de IP).
      </p>
      <p>
        Se quiser obter mais informações sobre marcadores e cookies associados com comerciais on-line ou rejeitar a coleta por terceiros dessas informações, visite o site da Network Advertising Initiative em <a href="http://www.networkadvertising.org" target="_blank">http://www.networkadvertising.org</a> (link externo).
      </p>
      <p>
        Também podemos usar os seguintes tipos de cookies comuns:
      </p>
      <ul>
        <li>
          Cookies de personalização da interface de usuário (ou seja, os cookies que memorizam suas preferências);
        </li>
        <li>
          Cookies de autenticação (por exemplo, cookies que permitem a você sair e voltar ao nosso site sem ter de realizar nova autenticação);
        </li>
        <li>
          Cookies de reprodutor de vídeo (ou seja, cookies que armazenam os dados necessários para reproduzir conteúdo de áudio ou vídeo e armazenar suas preferências).
        </li>
      </ul>

      <p>
        <strong>2.2</strong> Outras tecnologias
      </p>

      <p>
        Podemos também usar outras tecnologias nos nossos sites para coletar e processar seus dados pessoais para os mesmos propósitos, como acima referido, incluindo:
      </p>

      <ul>
        <li>
          Tecnologia Adobe Flash (incluindo Flash Local Shared Objects, a menos que você defina suas configurações diferentemente).
        </li>
      </ul>

      <p>
        <strong>5 Quais são seus direitos e como exercê-los?</strong>
      </p>

      <p>
        Você pode exercer os seguintes direitos, nas condições e dentro dos limites estabelecidos na lei.
      </p>

      <ul>
        <li>
          O direito de acessar seus dados pessoais processados por nós e, se você acreditar que qualquer informação sobre você é incorreta ou incompleta, obsoleta, ou para solicitar a sua correção ou atualização;
        </li>
        <li>
          O direito de requerer a eliminação dos seus dados pessoais ou a restrição para categorias específicas de processamento;
        </li>
        <li>
          O direito de retirar seu consentimento a qualquer momento, sem afetar a legalidade do tratamento antes de tal retirada de consentimento;
        </li>
        <li>
          O direito de se opor, no todo ou em parte, ao processamento de seus dados pessoais;
        </li>
        <li>
          O direito de se opor a marketing e comunicações diretas; e
        </li>
        <li>
          O direito de solicitar sua portabilidade, ou seja, que os dados pessoais que você forneceu a nós sejam devolvidos ou transferidos para a pessoa de sua escolha, de uma forma estruturada, comumente usada e de formato legível por máquina, sem qualquer impedimento de nosso lado e sujeito a suas obrigações de confidencialidade.
        </li>
      </ul>

      <p>
        Observe, contudo, que em determinadas circunstâncias, a sua recusa em aceitar os cookies ou configurações do navegador podem afetar a sua experiência de navegação e evitar a utilização de determinados recursos em nossos sites.
      </p>

      <p>
        Se você tiver uma pergunta ou deseja exercer os direitos acima, você pode enviar um e-mail para <a href="mailto:brasil.privacy_office@novartis.com">brasil.privacy_office@novartis.com</a> com uma digitalização de sua identidade para fins de identificação, sendo que nós utilizaremos esses dados apenas para verificar a sua identidade e não manteremos os mesmos após a leitura e a conclusão da verificação. Ao enviar-nos tal cópia digitalizada, por favor, certifique-se de eliminar de tal documento sua imagem e o número de identificação nacional ou equivalente da leitura.
      </p>

      <p>
        Se você não estiver satisfeito com a forma como processamos seus dados pessoais, dirija seu pedido ao nosso responsável pela proteção de dados <a href="mailto:global.privacy_office@novartis.com">global.privacy_office@novartis.com</a>, o qual investigará a sua preocupação.
      </p>

      <p>
        Em qualquer caso, você também tem o direito de apresentar uma queixa junto às autoridades competentes para a proteção de dados, além de seus direitos acima.
      </p>

      <p>
        <strong>3 Quais dados técnicos transacionais coletamos sobre você?</strong>
      </p>

      <p>
        <strong>3.1</strong> Categorias de dados técnicos e transacionais
      </p>

      <p>
        Além de todas as informações coletadas sobre você sob a Parte I da presente Política de Privacidade, nós podemos coletar vários tipos de dados técnicos e transacionais padrão dados pessoais sobre você durante seu uso de nossos sites que são necessários para garantir um bom funcionamento dos nossos sites, incluindo:
      </p>

      <ul>
        <li>
          Informações sobre o navegador e dispositivo (por exemplo, o domínio do provedor de internet, o tipo e a versão do browser, sistema operacional e plataforma, resolução de tela, o fabricante e o modelo do dispositivo);
        </li>
        <li>
          As estatísticas em relação ao seu uso de nosso site (por exemplo, informações sobre as páginas visitadas, as informações pesquisadas, tempo passado em nosso site);
        </li>
        <li>
          Uso de dados (ou seja, data e hora de acesso a nosso site, arquivos baixados);
        </li>
        <li>
          A localização do dispositivo através do Google Maps em nosso site (a menos que você tenha desativado este recurso, alterado as configurações do dispositivo ou tenha recusado o rastreamento de localização pelo Google Maps inserindo a sua localização por si mesmo); e
        </li>
        <li>
          De forma mais geral, qualquer informação que você forneça a nós em nosso site.
        </li>
      </ul>

      <p>
        Observe que nós não coletamos, utilizamos ou divulgamos dados pessoais de menores de idade 18 anos sem obter o consentimento de um dos pais ou responsável legal.
      </p>

      <p>
        <strong>3.2</strong> Por que coletamos dados técnicos e transacionais?
      </p>

      <p>
        Nós sempre processaremos os seus dados pessoais para uma finalidade específica e processaremos apenas os dados pessoais que são relevantes para a realização desse propósito. Adicionalmente a quaisquer fins já comunicados a você na parte I da presente Política de Privacidade, nós também processaremos os seus dados pessoais coletados durante a sua utilização de um de nossos sites para os seguintes propósitos:
      </p>

      <ul>
        <li>
          Gerenciar nossos usuários (por exemplo, registro, gestão de contas, responder a perguntas e prestar apoio técnico);
        </li>
        <li>
          Gerenciar e melhorar o nosso site (por exemplo, diagnosticar problemas de servidor, otimizar o tráfego, integrar e otimizar as páginas da web se necessário);
        </li>
        <li>
          Medir a utilização do nosso website (por exemplo, através da elaboração de estatísticas sobre o tráfego, através da coleta de informação sobre o comportamento dos usuários e as páginas que visitam);
        </li>
        <li>
          Aprimorar e personalizar a sua experiência e adequar melhor o conteúdo a você (por exemplo, ao lembrar as suas escolhas e preferências, utilizando cookies);
        </li>
        <li>
          Enviar serviços personalizados baseados em localização e conteúdo;
        </li>
        <li>
          Aprimorar a qualidade de nossos produtos e serviços e expandir nossas atividades de negócio;
        </li>
        <li>
          Monitorar e prevenir fraude, violação e outros abusos de nosso site;
        </li>
        <li>
          Resposta a um pedido oficial de uma autoridade judicial ou pública com a necessária autorização;
        </li>
        <li>
          Gerenciar nossos recursos de TI, incluindo o gerenciamento da infraestrutura e a continuidade de negócios;
        </li>
        <li>
          Preservar os interesses econômicos da empresa e garantir a conformidade e obrigações de informações (como, por exemplo, cumprir com as nossas políticas e requisitos legais locais, e deduções fiscais, gestão de alegação de casos de fraude, má conduta, realização de auditorias, defesa em litígio);
        </li>
        <li>
          arquivamento e manutenção de registros; e
        </li>
        <li>
          quaisquer outros fins impostos pela lei e pelas autoridades.
        </li>
      </ul>

      <p>
        <strong>Como você será informado sobre as alterações à nossa Política de Privacidade? </strong>
      </p>

      <p>
        Quaisquer alterações ou adições futuras ao processamento de seus dados pessoais, como descrito nesta Política de Privacidade, será notificada a você com antecedência, através de uma comunicação utilizando nossos canais de comunicação habituais (por exemplo, por e-mail), bem como através do site (através de banners, pop-ups ou outros mecanismos de notificação).
      </p>
    </div>

@endsection
