<!doctype html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index, follow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <base href="{{ url('/') }}">

    <meta name="author" content="Trupe Agência Criativa">
    <meta name="copyright" content="{{ date('Y') }} Trupe Agência Criativa">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta property="og:title" content="IgE 360 &middot; Concurso de casos clínicos">
    <meta property="og:description" content="">
    <meta property="og:site_name" content="IgE 360 &middot; Concurso de casos clínicos">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{ Request::url() }}">

    <title>IgE 360 &middot; Concurso de casos clínicos</title>

    <link rel="stylesheet" href="{{ url(mix('assets/css/main.css')) }}">
</head>
<body>
    @yield('header')
    @yield('content')
    @include('common.footer')

    <script src="{{ url(mix('assets/js/main.js')) }}"></script>
</body>
</html>
