<footer>
    <div class="center">
        <img src="{{ asset('assets/img/layout/rodape-infomec.svg') }}" alt="">
        <div class="info">
            <p>
                Material destinado exclusivamente a profissionais de saúde habilitados a prescrever e/ou dispensar medicamentos.<br>
                2020 © - Direitos Reservados - Novartis Biociências S/A. Proibida a reprodução total ou parcial sem a autorização do titular.
            </p>
            <p>
                Abril/2020<br>
                BR-08673
            </p>
        </div>
        <div class="empresas">
            <img src="{{ asset('assets/img/layout/rodape-novartis.svg') }}" alt="">
            <p>
                Novartis Biociências S.A.<br>
                Setor Farma - Av. Prof. Vicente Rao, 90<br>
                São Paulo, SP - CEP 04636-000<br>
                <a href="http://www.novartis.com.br">www.novartis.com.br</a><br>
                <a href="http://www.portalnovartis.com.br">www.portalnovartis.com.br</a>
            </p>
            <p>
                Infomec<br>
                Informações Médico Científicas<br>
                <a href="mailto:infomec.novartis@novartis.com">infomec.novartis@novartis.com</a><br>
                0800 888 3003 (fixo)<br>
                11 3253 3405 (cel)
            </p>
        </div>
        <div class="copyright">
            <p>
                SIC | Serviço de Informações ao Cliente<br>
                <a href="mailto:sic.novartis@novartis.com">sic.novartis@novartis.com</a>
            </p>
            <p>
                O uso deste site é governado por nossos<br>
                <a href="https://www.novartis.com.br/termos-de-uso" target="_blank">Termos de Uso</a>
                e nosso
                <a href="{{ route('aviso-de-privacidade') }}">Aviso de Privacidade</a>.
            </p>
            <p>
                © 2020 Novartis Biociências S.A.
            </p>
        </div>
    </div>
</footer>
