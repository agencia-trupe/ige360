@auth('medico')
<div class="medico-usuario">
    <div class="center">
        <h3>Olá Dr.(a) {{ auth('medico')->user()->nome }}</h3>
        <a href="{{ route('medico.logout') }}" class="logout">SAIR</a>
    </div>
</div>
@endauth

<header class="medico">
    <div class="logo @auth('medico') logo-small @endauth">
        <img src="{{ asset('assets/img/layout/marca-concurso-ige360-bco.svg') }}" alt="">
    </div>

    @auth('medico')
    <div class="center">
        <nav class="medico-nav">
            <a href="{{ route('medico.regulamento') }}" @if(Str::is('medico.regulamento*', Route::currentRouteName())) class="active" @endif>
                REGULAMENTO
            </a>
            <a href="{{ route('medico.cronograma') }}" @if(Str::is('medico.cronograma*', Route::currentRouteName())) class="active" @endif>
                CRONOGRAMA
            </a>
            <a href="{{ route('medico.avaliadores') }}" @if(Str::is('medico.avaliadores*', Route::currentRouteName())) class="active" @endif>
                AVALIADORES
            </a>
            <a href="{{ route('medico.caso-clinico') }}" @if(Str::is('medico.caso-clinico*', Route::currentRouteName())) class="active" @endif>
                CASO CLÍNICO
                @if(env('EXIBIR_RESULTADO'))
                    <span class="resultado">RESULTADO</span>
                @else
                    <span>SUBMISSÃO</span>
                @endif
            </a>
        </nav>
    </div>
    @endauth
</header>

