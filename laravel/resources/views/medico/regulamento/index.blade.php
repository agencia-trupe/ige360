@extends('medico.common.template')

@section('content')

    <div class="main-padding medico-regulamento">
        <div class="center">
            @include('common.flash')

            <div class="regulamento-row">
                <div class="texto">
                    <p>
                        O Concurso de Casos Clínicos envolverá o envio de relatos de casos clínicos sobre o tratamento da Urticária Crônica Espontânea (UCE) com omalizumabe, por grupos de médicos residentes de centros de dermatologia e alergia do Brasil.
                    </p>
                    <p>
                        O objetivo é difundir o conhecimento técnico e científico sobre o tratamento da UCE com o uso omalizumabe e permitir o intercâmbio de boas práticas entre os dermatologistas e alergistas do Brasil.
                    </p>
                </div>
            </div>
            <div class="regulamento-row">
                <h2>TEMA DE 2020</h2>
                <div class="texto">
                    <h3>Experiência clínica com omalizumabe no tratamento da UCE.</h3>
                </div>
            </div>
            <div class="regulamento-row">
                <h2>AVALIAÇÃO</h2>
                <div class="texto">
                    <p>
                        O <strong>Comitê avaliador</strong> será formado pela RUBRA (Rede Urticária Brasil): médicos alergistas líderes nacionais, os quais atuarão com total independência em relação à Novartis.
                    </p>
                    <ul>
                        <li>
                            Dra. Roberta Criado - SP 67350
                        </li>
                        <li>
                            Dr. Luis Felipe Ensina - SP 86757
                        </li>
                        <li>
                            Dr. Regis Campos - BA 16126
                        </li>
                        <li>
                            Dra. Rosana Agondi - SP 59444
                        </li>
                        <li>
                            Dra. Solange Valle - RJ 439056
                        </li>
                        <li>
                            Dra. Karla Arruda - SP 50494
                        </li>
                    </ul>
                </div>
            </div>
            <div class="regulamento-row">
                <h2>CRITÉRIOS DE AVALIAÇÃO</h2>
                <div class="texto">
                    <p>
                        Os critérios de avaliação da Banca Julgadora para definição dos vencedores serão:
                    </p>
                    <ul>
                        <li>
                            (i) Título;
                        </li>
                        <li>
                            (ii) Relato do caso clínico;
                        </li>
                        <li>
                            (iii) Discussão e conclusão.
                        </li>
                    </ul>
                    <p>
                        Os casos serão avaliados de forma anônima pela Banca Julgadora, ou seja, a Banca não terá informação do nome dos Centros, médicos, hospital ou cidade do relato do caso avaliado. A Plataforma anonimizará referidos casos, possibilitando a análise isenta da Banca.
                    </p>
                    <p>
                        Serão escolhidos 2 Centros vencedores, pela Banca Julgadora, um de alergia e outro de dermatologia, sendo que esses deverão, obrigatoriamente, ter no mínimo os critérios mencionados acima, ficando a análise de referidos critérios e definição dos vencedores na responsabilidade da <strong>RUBRA</strong>.
                    </p>
                </div>
            </div>
            <div class="regulamento-row">
                <h2>PREMIAÇÃO</h2>
                <div class="texto">
                    <p><strong>Centro de Dermatologia</strong></p>
                    <p>
                        O Centro, formado por 1 (um) coordenador e até 5 (cinco) médicos residentes, receberá até 6 (seis) inscrições do Congresso Brasileiro de Dermatologia, dependendo do número de participantes do Centro, que será realizado em 2021 (data a ser definida).
                    </p>
                    <p style="margin-top:2em"><strong>Centro de Alergia</strong></p>
                    <p>
                        O Centro, formado por 1 (um) coordenador e até 5 (cinco) médicos residentes, receberá até 6 (seis) inscrições do Congresso Brasileiro de Alergia e Imunologia, dependendo do número de participantes do Centro, que será realizado em 2021 (data a ser definida), além de apresentar o caso clínico vencedor em simpósio da Novartis durante o Congresso Brasileiro de Alergia e Imunologia.
                    </p>
                </div>
            </div>
            <div class="regulamento-row">
                <h2>CRONOGRAMA</h2>
                <div class="texto">
                    <ul>
                        <li>
                            <strong>3 de agosto a 30 de outubro</strong>: divulgação e submissão dos casos clínicos para avaliação.
                        </li>
                        <li>
                            <strong>3 a 6 de novembro</strong>: avaliação dos relatos de casos clínicos pela Farmacovigilância Novartis.
                        </li>
                        <li>
                            <strong>11 a 13 de novembro</strong>: avaliação dos relatos de casos clínicos pela Área Médica Novartis.
                        </li>
                        <li>
                            <strong>16 a 20 de novembro</strong>: avaliação dos relatos de casos clínicos pela RUBRA e definição dos vencedores.
                        </li>
                        <li>
                            <strong>7 de dezembro de 2020</strong>: anúncio dos vencedores.
                        </li>
                    </ul>
                </div>
            </div>
            <div class="regulamento-row">
                <h2>REGRAS</h2>
                <div class="texto">
                    <ul>
                        <li>
                            É essencial preservar a identidade dos pacientes: não usar o nome ou iniciais do paciente e omitir detalhes que possam identificar as pessoas (caso não sejam essenciais para o relato do caso).
                        </li>
                        <li>
                            Necessário ter o consentimento, por escrito, do paciente para que seus dados e imagens sejam utilizados no relato do caso clínico submetido ao <strong>Programa de Educação Médica – Concurso de Casos Clínicos</strong>.
                        </li>
                        <li>
                            Informações de pacientes deverão ser anônimas como forma de não ser sua identificação.
                        </li>
                        <li>
                            Por conta do volume de trabalhos e da necessidade legal de reporte dos eventos adversos aos órgãos regulatórios nacionais e globais pela área de farmacovigilância da Novartis, haverá quatro datas-limite (8, 15, 22 e 29 de julho) como prazo de submissão entre os grupos, e essa definição ocorrerá por meio de um sorteio a ser realizado.
                        </li>
                        <li>
                            As informações que caracterizam um relato de evento adverso ou cenário especial envolvendo qualquer medicamento do grupo Novartis serão coletadas, armazenadas e processadas pela Novartis para monitoramento de segurança dos nossos produtos e, quando for preciso, tais informações serão enviadas à autoridade sanitária local e global, conforme legislação vigente.
                        </li>
                        <li>
                            Se um relato de evento adverso ou cenário especial for feito durante o <strong>Concurso de Casos Clínicos</strong>, o departamento de Farmacovigilância da Novartis poderá entrar em contato para obter informações adicionais.
                        </li>
                    </ul>
                </div>
            </div>
            <div class="regulamento-row">
                <h2>DESCLASSIFICAÇÃO DOS CASOS</h2>
                <div class="texto">
                    <p>
                        Serão <strong style="text-decoration:underline">desclassificados automaticamente</strong> e não avaliados os casos clínicos de pacientes que:
                    </p>
                    <ul>
                        <li>
                            Tenham sido submetidos a uso off-label (para mais informações, acessar a bula de Xolair, aprovada pela Anvisa em 17/06/2019).
                        </li>
                        <li>
                            Sejam oriundos de estudo clínico.
                        </li>
                        <li>
                            Não estiverem em uso de omalizumabe no momento da submissão do caso.
                        </li>
                    </ul>
                    <p>
                        Uma vez que as regras estão estabelecidas no regulamento, a Novartis se reserva ao direito de não comunicar a desclassificação.
                    </p>
                </div>
            </div>

            @if(!auth('medico')->user()->termos_aceitos_em)
                <form action="{{ route('medico.regulamento.post') }}" class="termo-de-consentimento" method="POST">
                    @csrf

                    <h2>TERMO DE CONSENTIMENTO</h2>
                    <ul>
                        <li>
                            LI e CONCORDO com o Regulamento do Concurso de Casos Clínicos IgE 360 da Novartis (acima).
                        </li>
                        <li>
                            LI e COMPREENDI as informações do <a href="{{ route('aviso-de-privacidade') }}" target="_blank">Aviso de Privacidade</a> do Concurso de Casos Clínicos IgE 360 da Novartis relativas ao tratamento dos meus dados pessoais.
                        </li>
                        <li>
                            CONCORDO com o uso dos meus dados pessoais para participar do Concurso de Casos Clínicos IgE 360 da Novartis, de acordo com o <a href="{{ route('aviso-de-privacidade') }}" target="_blank">Aviso de Privacidade</a>.
                        </li>
                    </ul>

                    <label class="checkbox">
                        <input type="checkbox" name="consentimento" value="1">
                        <span>Estou de acordo com os termos acima e desejo participar do Concurso de Casos Clínicos IgE 360 da Novartis.</span>
                    </label>

                    <input type="submit" class="btn btn-medico" value="CONCORDO">
                </form>
            @endif
        </div>
    </div>

@endsection
