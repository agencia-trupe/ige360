@extends('medico.common.template')

@section('content')

    <div class="main-padding medico-cronograma">
        <div class="center">
            <h1>CRONOGRAMA</h1>

            <div class="grid">
                <div>
                    <img src="{{ asset('assets/img/layout/crono-data1.png') }}" alt="">
                    <h2>3 DE AGOSTO A 30 DE OUTUBRO</h2>
                    <p>Submissão dos casos clínicos para avaliação.</p>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/crono-data2.png') }}" alt="">
                    <h2>3 A 6 DE NOVEMBRO</h2>
                    <p>Avaliação dos relatos de casos clínicos pela Farmacovigilância - Novartis.</p>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/crono-data3.png') }}" alt="">
                    <h2>11 A 13 DE NOVEMBRO</h2>
                    <p>Avaliação Área Médica Novartis.</p>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/crono-data4.png') }}" alt="">
                    <h2>16 A 20 DE NOVEMBRO</h2>
                    <p>Avaliação RUBRA e definição dos vencedores.</p>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/crono-data5.png') }}" alt="">
                    <h2>7 DE DEZEMBRO DE 2020</h2>
                    <p>Anúncio dos Vencedores.</p>
                </div>
            </div>
        </div>
    </div>

@endsection
