@extends('medico.common.template')

@section('content')

    <div class="main-padding medico-caso-clinico">
        <div class="center">
            <h1>RESULTADO</h1>

            @if($vencedor)
                <div class="vencedor">
                    <span>VENCEDOR</span>
                    <div>
                        <h2>{{ $vencedor->titulo }}</h2>
                        <p>1&ordm; LUGAR | {{ $vencedor->nota_resultado }} pontos:</p>
                        <h3>Dr.(a) {{ $vencedor->medico->nome }} - CRM {{ $vencedor->medico->crm }}</h3>
                    </div>
                </div>
            @endif

            @if($casoClinico && $casoClinico->nota && (!$vencedor || $vencedor->id !== $casoClinico->id))
                <div class="pontuacao">
                    A SUA PONTUAÇÃO FOI DE
                    <strong>{{ $casoClinico->nota }}</strong>
                    PONTOS.
                </div>
            @endif
        </div>
    </div>

@endsection
