@extends('medico.common.template')

@section('content')

    <div class="main-padding medico-caso-clinico">
        <div class="center">
            <h1>CASO CLÍNICO ENVIADO</h1>

            <div class="caso-enviado" style="line-height:1.5">
                VOCÊ ENVIOU SEU CASO CLÍNICO EM
                {{ $casoClinico->enviado_em->format('d/m/Y \\à\\s H:i \\h') }}

                <br><br>

                Agradecemos a participação. Ao final do prazo de submissões iniciaremos as avaliações. Aguarde a divulgação dos resultados.
            </div>
        </div>
    </div>

@endsection
