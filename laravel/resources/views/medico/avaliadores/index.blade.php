@extends('medico.common.template')

@section('content')

    <div class="main-padding medico-avaliadores">
        <div class="center">
            <h1>AVALIADORES</h1>

            <div class="grid">
                <div>
                    <img src="{{ asset('assets/img/layout/avaliador1-RegisCampos.png') }}" alt="">
                    <div>
                        <h2>Dr. Regis Campos</h2>
                        <ul>
                            <li>Médico Alergista e Imunologista clínico.</li>
                            <li>Professor da Faculdade de Medicina da Universidade Federal da Bahia.</li>
                            <li>Coordenador do Departamento Científico de Urticaria da ASBAI.</li>
                            <li>Membro da Rede Urticaria Brasil (Rubra).</li>
                            <li>Coordenador do centro UCARE do Hospital Universitário Prof Edgar Santos.</li>
                        </ul>
                    </div>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/avaliador2-RobertaCriado.png') }}" alt="">
                    <div>
                        <h2>Dra. Roberta Criado</h2>
                        <ul>
                            <li>Responsável pelo Ambulatório de Urticária da disciplina de Dermatologia da FMABC.</li>
                        </ul>
                    </div>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/avaliador3-SolangeValle.png') }}" alt="">
                    <div>
                        <h2>Dra. Solange Valle</h2>
                        <ul>
                            <li>Mestre e doutora pela Universidade Federal do Rio de Janeiro (UFRJ).</li>
                            <li>Coordenadora do Centro de Referência e Excelência em Urticária e Angioedema - UCARE/ACARE.</li>
                            <li>Chefe do Serviço de Imunologia do Hospital Universitário Clementino Fraga Filho/UFRJ.</li>
                            <li>Diretora Científica da ASBAI RJ.</li>
                            <li>Membro do Departamento Científico de Urticária /Angioedema da ASBAI.</li>
                        </ul>
                    </div>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/avaliador4-RosanaAgondi.png') }}" alt="">
                    <div>
                        <h2>Dra. Rosana Agondi</h2>
                        <ul>
                            <li>Médica assistente do Serviço de Imunologia Clinica e Alergia do Hospital das Clínicas da FMUSP.</li>
                        </ul>
                    </div>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/avaliador5-KarlaArruda.png') }}" alt="">
                    <div>
                        <h2>Dra. Karla Arruda</h2>
                        <ul>
                            <li>Professora Titular de Clínica Médica.</li>
                            <li>Faculdade de Medicina de Ribeirão Preto da Universidade de São Paulo.</li>
                            <li>Médica Especialista em Alergia e Imunologia pela ASBAI.</li>
                        </ul>
                    </div>
                </div>
                <div>
                    <img src="{{ asset('assets/img/layout/avaliador6-LuisEnsina.png') }}" alt="">
                    <div>
                        <h2>Dra. Karla Arruda</h2>
                        <ul>
                            <li>Graduado em Medicina pela Faculdade de Medicina de Sorocaba da PUC SP e mestrado em Imunologia pela USP.</li>
                            <li>Investigador principal do Centro de Pesquisa Clínica CPAlpha, desde 2010.</li>
                            <li>Coordenador dos Centros de Referência e Excelência em Urticária (UCARE) da UNIFESP e CPAlpha.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
