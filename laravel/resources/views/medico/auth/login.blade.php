@extends('medico.common.template')

@section('content')

    <div class="auth medico">
        <div class="auth-content center">
            <p>
                Prezado(a) Dr(a), seja bem-vindo(a) ao website do
                <strong>Concurso de casos clínicos IgE360</strong>.<br>
                Faça o login para acessar mais informações.
            </p>

            <form method="POST" action="{{ route('medico.login.post') }}">
                @csrf

                @error('email')
                <div class="flash flash-error">
                    Login e senha inválidos.
                </div>
                @enderror

                <input type="email" name="email" value="{{ old('email') }}" placeholder="login (e-mail)" required autofocus>
                <input type="password" name="password" placeholder="senha" required>
                <button type="submit">ACESSAR SISTEMA</button>
            </form>

            <a class="esqueci" href="{{ route('medico.password.request') }}">
                esqueci minha senha &raquo;
            </a>
        </div>
    </div>

@endsection
