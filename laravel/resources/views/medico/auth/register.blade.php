@extends('medico.common.template')

@section('content')

    <div class="auth medico">
        <div class="auth-content center">
            <p>
                Prezado(a) Dr(a), seja bem-vindo(a) ao website do
                <strong>Concurso de casos clínicos IgE360</strong>.<br>
                Faça o login para acessar mais informações.
            </p>

            <p>SEU E-MAIL: {{ request('email') }}</p>

            <form method="POST" action="{{ route('medico.register.post') }}">
                @csrf

                @include('common.flash')

                <input type="hidden" name="token" value="{{ request('token') }}">
                <input type="hidden" name="email" value="{{ request('email') }}">
                <input type="password" name="password" placeholder="senha" required>
                <input type="password" name="password_confirmation" placeholder="confirmar senha" required>
                <button type="submit">ACESSAR SISTEMA</button>
            </form>
        </div>
    </div>

@endsection
