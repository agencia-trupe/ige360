@extends('medico.common.template')

@section('content')

    <div class="auth medico">
        <div class="auth-content center">
            <p>REDEFINIR SENHA</p>

            <form method="POST" action="{{ route('medico.password.update') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">
                <input type="hidden" name="email" value="{{ $email }}">

                @error('password')
                    <div class="flash flash-error">
                        {{ $message }}
                    </div>
                @enderror
                @error('email')
                    <div class="flash flash-error">
                        {{ $message }}
                    </div>
                @enderror

                <input type="password" name="password" placeholder="senha" required autofocus>
                <input type="password" name="password_confirmation" placeholder="confirmar senha" required>

                <button type="submit">REDEFINIR SENHA</button>
            </form>

            <a href="{{ route('medico.login') }}" class="esqueci">
                &laquo; voltar
            </a>
        </div>
    </div>

@endsection
