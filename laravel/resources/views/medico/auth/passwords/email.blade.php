@extends('medico.common.template')

@section('content')

    <div class="auth medico">
        <div class="auth-content center">
            <p>ESQUECI MINHA SENHA</p>

            <form method="POST" action="{{ route('medico.password.email') }}">
                @csrf

                @if(session('status'))
                    <div class="flash flash-success">
                        Um e-mail foi enviado com instruções para a redefinição de senha.
                    </div>
                @endif
                @error('email')
                    <div class="flash flash-error">
                        {{ $message }}
                    </div>
                @enderror

                <input type="email"name="email" value="{{ old('email') }}" required placeholder="e-mail" autofocus>
                <button type="submit">REDEFINIR SENHA</button>
            </form>

            <a href="{{ route('medico.login') }}" class="esqueci">
                &laquo; voltar
            </a>
        </div>
    </div>

@endsection
