<header class="avaliador">
    <div class="logo">
        <img src="{{ asset('assets/img/layout/marca-concurso-ige360-marinho.svg') }}" alt="">
    </div>

    @auth('avaliador')
    <div class="user">
        <div class="center">
            <h2>AVALIADOR</h2>
            <h3>Olá {{ auth('avaliador')->user()->nome }}</h3>
            <a href="{{ route('avaliador.logout') }}" class="logout">SAIR</a>
        </div>
    </div>
    @endauth
</header>

@auth('avaliador')
<nav class="main-nav avaliador">
    <div class="center">
        <a href="{{ route('avaliador.casos-clinicos') }}" @if(Str::is('avaliador.casos-clinicos*', Route::currentRouteName())) class="active" @endif>
            CASOS CLÍNICOS
        </a>
        <a href="{{ route('avaliador.historico') }}" @if(Str::is('avaliador.historico*', Route::currentRouteName())) class="active" @endif>
            HISTÓRICO
        </a>
    </div>
</nav>
@endauth
