@extends('avaliador.common.template')

@section('content')

    <div class="auth avaliador">
        <div class="title">
            <h1>AVALIADOR</h1>
        </div>

        <div class="auth-content center">
            <p>ESQUECI MINHA SENHA</p>

            <form method="POST" action="{{ route('avaliador.password.email') }}">
                @csrf

                @if(session('status'))
                    <div class="flash flash-success">
                        Um e-mail foi enviado com instruções para a redefinição de senha.
                    </div>
                @endif
                @error('email')
                    <div class="flash flash-error">
                        {{ $message }}
                    </div>
                @enderror

                <input type="email"name="email" value="{{ old('email') }}" required placeholder="e-mail" autofocus>
                <button type="submit">REDEFINIR SENHA</button>
            </form>

            <a href="{{ route('avaliador.login') }}" class="esqueci">
                &laquo; voltar
            </a>
        </div>
    </div>

@endsection
