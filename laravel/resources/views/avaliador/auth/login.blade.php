@extends('avaliador.common.template')

@section('content')

    <div class="auth avaliador">
        <div class="title">
            <h1>AVALIADOR</h1>
        </div>

        <div class="auth-content center">
            <p>No primeiro acesso clique em "ESQUECI MINHA SENHA" para receber o e-mail com link para definição de senha.</p>

            <form method="POST" action="{{ route('avaliador.login.post') }}">
                @csrf

                @error('email')
                <div class="flash flash-error">
                    Login e senha inválidos.
                </div>
                @enderror

                <input type="email" name="email" value="{{ old('email') }}" placeholder="login (e-mail)" required autofocus>
                <input type="password" name="password" placeholder="senha" required>
                <button type="submit">ACESSAR SISTEMA</button>
            </form>

            <a class="esqueci" href="{{ route('avaliador.password.request') }}">
                esqueci minha senha &raquo;
            </a>
        </div>
    </div>

@endsection
