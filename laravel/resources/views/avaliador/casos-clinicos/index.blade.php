@extends('avaliador.common.template')

@section('content')

    <div class="main-padding avaliador-casos-clinicos">
        <div class="center">
            @include('common.flash')

            @if(count($casosClinicos))
                <div class="casos-lista">
                    <div class="grid">
                        <span>TÍTULO DO CASO CLÍNICO:</span>
                        <span>DATA DE RECEBIMENTO:</span>
                    </div>

                    @foreach($casosClinicos as $caso)
                        <div class="grid">
                            <div class="cell">{{ $caso->titulo }}</div>
                            <div class="cell">
                                {{ $caso->enviado_em->format('d/m/y - H:i \\h') }}
                            </div>
                            <a href="{{ route('avaliador.casos-clinicos', $caso->id) }}" class="btn-caso">AVALIAR</a>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="nenhum">Nenhum caso clínico encontrado.</div>
            @endif
        </div>
    </div>

@endsection
