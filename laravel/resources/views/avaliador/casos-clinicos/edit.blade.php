@extends('avaliador.common.template')

@section('content')

    <div class="main-padding avaliador-casos-clinicos">
        <div class="center">
            @include('common.flash')

            <div class="form-caso">
                <div class="row-caso">
                    <label>título do caso clínico</label>
                    <div class="text-editor-readonly">
                        <p>{{ $casoClinico->titulo }}</p>
                    </div>
                </div>
                <div class="row-caso">
                    <label>relato do caso clínico</label>
                    <div class="text-editor-readonly">{!! $casoClinico->relato !!}</div>
                </div>
                <div class="row-caso">
                    <label>conclusão</label>
                    <div class="text-editor-readonly">{!! $casoClinico->conclusao !!}</div>
                </div>
                <div class="row-caso">
                    <label>referências bibliográficas</label>
                    <div class="text-editor-readonly">{!! $casoClinico->referencias_bibliograficas !!}</div>
                </div>

                <form action="{{ route('avaliador.casos-clinicos.post', $casoClinico->id) }}" method="POST" class="form-nota">
                    @csrf

                    <span>NOTA:</span>
                    <input type="text" name="nota" id="input-nota" placeholder="0,00" required>
                    <input type="submit" value="APLICAR NOTA" class="btn-enviar-confirmacao" data-confirmacao="Deseja apicar esta nota ao caso clínico? Essa ação não pode ser desfeita.">
                </form>
            </div>
        </div>
    </div>

@endsection
