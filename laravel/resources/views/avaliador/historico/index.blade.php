@extends('avaliador.common.template')

@section('content')

    <div class="main-padding avaliador-historico">
        <div class="center">
            @if(count($casosClinicos))
                <div class="casos-lista">
                    <div class="grid">
                        <span>TÍTULO DO CASO CLÍNICO:</span>
                        <span>DATA DE RECEBIMENTO:</span>
                        <span>DATA DE AVALIAÇÃO:</span>
                    </div>

                    @foreach($casosClinicos as $caso)
                        <div class="grid">
                            <div class="cell">{{ $caso->titulo }}</div>
                            <div class="cell">
                                {{ $caso->enviado_em->format('d/m/y - H:i \\h') }}
                            </div>
                            <div class="cell">
                                {{ $caso->avaliado_em
                                    ? $caso->avaliado_em->format('d/m/y - H:i \\h')
                                    : '-'
                                }}
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                <div class="nenhum">Nenhum caso clínico encontrado.</div>
            @endif
        </div>
    </div>

@endsection
